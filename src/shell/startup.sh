#!/bin/bash
# add the libraries to the INSTALL_CLASSPATH.
# SCRIPT_PATH is the directory where this executable is.
# load app config from app.info.conf and profiles.active.conf

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
APP_PATH=${SCRIPT_PATH}/..
CONFIG_PATH=${APP_PATH}/conf

source ${CONFIG_PATH}/config.shlib

# load from custom config
APP_NAME="$(config_get appName)"
EXECUTOR="$(config_get executor)"
# load from custom config

APP_OUT=${APP_PATH}/logs/${APP_NAME}.out

mkdir -p ${APP_PATH}/logs

for i in ${DIR_LIBS}
do
  if [ -z "${RUN_CLASSPATH}" ] ; then
    RUN_CLASSPATH=$i
  else
    RUN_CLASSPATH="$i":${RUN_CLASSPATH}
  fi
done

DIR_LIBS=${APP_PATH}/lib/*.zip
for i in ${DIR_LIBS}
do
  if [ -z "${RUN_CLASSPATH}" ] ; then
    RUN_CLASSPATH=$i
  else
    RUN_CLASSPATH="$i":${RUN_CLASSPATH}
  fi
done
export CLASSPATH=${RUN_CLASSPATH}:${CLASSPATH}:${JAVA_HOME}/lib/tools.jar
#echo ${APP_PATH}
#export JAVA_OPTS=$JAVA_OPTS -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8787
#	-Dlogging.config=${APP_PATH}/logback.xml \
# --spring.config.location=${APP_PATH}/application-pro.yml \

eval exec nohup java \
	-classpath ${JAVA_HOME}/lib/tools.jar \
	--add-modules=ALL-SYSTEM \
	-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${APP_PATH}  \
	-DappName=${APP_NAME} \
	-DappPath=${APP_PATH} \
	-DappOut=${APP_OUT} \
	-Dlogging.config=${APP_PATH}/conf/logback.xml \
	-Dredisson.config=${APP_PATH}/conf/redisson.yml \
	-Dapp-profile=pro \
	${EXECUTOR} \
	--spring.config.location=${APP_PATH}/conf/application.yml,${APP_PATH}/conf/application-pro.yml \
	>> ${APP_OUT}2>&1 & 
	
echo "start ${APP_NAME} finished "
