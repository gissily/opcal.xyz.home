#!/bin/bash
# add the libraries to the INSTALL_CLASSPATH.
# SCRIPT_PATH is the directory where this executable is.
# load app config from app.info.conf and profiles.active.conf

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "$SCRIPT")
APP_PATH=${SCRIPT_PATH}/..
CONFIG_PATH=${APP_PATH}/conf

source ${CONFIG_PATH}/config.shlib

# load from custom config
APP_NAME="$(config_get appName)"
EXECUTOR="$(config_get executor)"
BREAKTIME="$(config_get breakTime)"
# load from custom config


if [[ ${BREAKTIME} -gt 0 ]];then
    BREAKTIME=${BREAKTIME}
else
    BREAKTIME=60
fi  


PID=`ps -ef |grep java |grep  $APP_NAME |grep -v grep |grep -v tail | awk '{print $2}'`

echo "App ${PID} will be shutdown"

kill ${PID}

while true
do
PID=`ps -ef |grep java |grep  $APP_NAME |grep -v grep |grep -v tail | awk '{print $2}'`


if [[ -n "${PID}" && ${BREAKTIME} -gt 0 ]];then
        BREAKTIME=$(($BREAKTIME-10))
    echo "Process ${PID} is still alive. It will be killed after ${BREAKTIME} seconds"
    sleep 10
elif [[ -n "${PID}" && ${BREAKTIME} -le 0 ]];then
        kill -9 ${PID}
        break
else    
        break
fi
done