const gulp = require("gulp");
const gap = require("gulp-append-prepend");

gulp.task("spring", async function () {
	
  gulp
    .src("build/**", {ignore: ["build/index.html"]})
    .pipe(gulp.dest("../../../target/classes/static/", { overwrite: true }));

  gulp
    .src("build/index.html")
    .pipe(gulp.dest("../../../target/classes/templates/", { overwrite: true }));
  return;
});
