import React from "react";

import IndexNavbar from "components/Navbars/IndexNavbar.js";


function E500() {
	document.documentElement.classList.remove("nav-open");
	React.useEffect(() => {
		document.body.classList.add("index");
		return function cleanup() {
			document.body.classList.remove("index");
		};
	});
	return (
		<>
			<IndexNavbar />
			<div className="page-header error500-container">
				<button class="error-code">
					<div class="number five">
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>

						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
					</div>
					<div class="number zero">
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
					</div>
					<div class="number zero">
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>

						<div class="cell empty"></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell filled"><div class="bug"></div></div>
						<div class="cell empty"></div>
					</div>
				</button>
			</div>
		</>
	);
}

export default E500;