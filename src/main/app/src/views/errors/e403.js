import React from "react";

import IndexNavbar from "components/Navbars/IndexNavbar.js";


function E403() {
	document.documentElement.classList.remove("nav-open");
	React.useEffect(() => {
		document.body.classList.add("index");
		return function cleanup() {
			document.body.classList.remove("index");
		};
	});
	return (
		<>
			<IndexNavbar />
			<div className="page-header error404-container">
				<div class="error403">
					<div>403</div>
					<div class="txt">
						Forbidden<span class="blink">_</span>
					</div>
				</div>
			</div>
		</>
	);
}

export default E403;