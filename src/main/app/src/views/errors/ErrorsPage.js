import React from 'react';
import { connect } from 'react-redux';
import E403 from 'views/errors/e403';
import E404 from 'views/errors/e404';
import E500 from 'views/errors/e500';



const mapStateToProps = state => ({
	errors: state.errors
})


const Errors = ({ errors }) => {
	switch (errors.status) {
		case 500:
			return <E500 />;
		case 403:
			return <E403 />;
		case 404:
		default:
			return <E404 />;
	}
};

export default connect(mapStateToProps)(Errors);

