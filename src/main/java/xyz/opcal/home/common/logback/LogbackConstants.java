package xyz.opcal.home.common.logback;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LogbackConstants {

	public static final String MDC_THREAD_ID = "mdcThreadId";
	public static final String CURRENT_THREAD_ID = "currentThreadId";
}
