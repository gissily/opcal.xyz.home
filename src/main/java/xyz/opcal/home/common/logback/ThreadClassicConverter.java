package xyz.opcal.home.common.logback;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class ThreadClassicConverter extends ClassicConverter {

	@Override
	public String convert(ILoggingEvent event) {
		String systemId = event.getMDCPropertyMap().get(LogbackConstants.MDC_THREAD_ID);
		if (StringUtils.isBlank(systemId)) {
			systemId = UUID.nameUUIDFromBytes(String.valueOf(Thread.currentThread().getId()).getBytes()).toString().replace("-", "");
		}
		return systemId;
	}

}
