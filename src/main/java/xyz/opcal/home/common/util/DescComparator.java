package xyz.opcal.home.common.util;

import java.util.Comparator;

public class DescComparator<T> implements Comparator<T> {

	private Comparator<T> comparator;

	public DescComparator(Comparator<T> comparator) {
		this.comparator = comparator;
	}

	@Override
	public int compare(T o1, T o2) {
		return comparator.compare(o2, o1);
	}

	public static <T> DescComparator<T> descComparator(Comparator<T> comparator) {
		return new DescComparator<>(comparator);
	}

}
