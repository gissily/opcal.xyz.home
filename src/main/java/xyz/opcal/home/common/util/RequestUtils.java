package xyz.opcal.home.common.util;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.util.InetAddressUtils;

import lombok.experimental.UtilityClass;
import xyz.opcal.home.common.HomeConstants;

@UtilityClass
public class RequestUtils {

	public static final String B_H_REG_EX = "[\\n\\r\\t]";
	public static final String B_H_REG_EX_REPLACEMENT = "_";

	public static String cleanTaint(String value) {
		return RegExUtils.replaceAll(value, B_H_REG_EX, B_H_REG_EX_REPLACEMENT);
	}

	public static String getIp(@NotNull HttpServletRequest request) {
		String workerClientIp = request.getHeader(HomeConstants.W_HEADER_CONNECTING_IP);
		String ip = StringUtils.isNotBlank(workerClientIp) ? workerClientIp : request.getHeader(HomeConstants.CF_HEADER_CONNECTING_IP);
		if (StringUtils.isBlank(ip)) {
			ip = request.getHeader(HomeConstants.X_FORWARDED_FOR);
		}
		if (StringUtils.isBlank(ip) || StringUtils.equals(ip, HomeConstants.LOCALHOST_IP)) {
			ip = getRealIp(cleanTaint(request.getHeader(HomeConstants.X_REAL_IP)));
		}
		if (StringUtils.isBlank(ip) || !isIp(ip)) {
			ip = request.getRemoteAddr();
		}
		return cleanTaint(ip);
	}

	public static boolean fromWorker(@NotNull HttpServletRequest request) {
		return StringUtils.isNotBlank(cleanTaint(request.getHeader(HomeConstants.W_HEADER_CONNECTING_IP)));
	}

	static String getRealIp(String realIpString) {
		if (realIpString == null) {
			return null;
		}
		String[] ips = StringUtils.split(realIpString, ",");
		for (int i = ips.length - 1; i >= 0; i--) {
			String ip = ips[i];
			if (!StringUtils.equals(ip, HomeConstants.LOCALHOST_IP)) {
				return ip;
			}
		}
		return null;
	}

	public static boolean isIp(String ipString) {
		return ipString != null && (InetAddressUtils.isIPv4Address(ipString) || InetAddressUtils.isIPv6Address(ipString));
	}

	public static String getCountryCode(@NotNull HttpServletRequest request) {
		String workerClientCode = request.getHeader(HomeConstants.W_HEADER_IP_COUNTRY);
		String countryCode = StringUtils.isNotBlank(workerClientCode) ? workerClientCode : request.getHeader(HomeConstants.CF_HEADER_IP_COUNTRY);
		return cleanTaint(countryCode);
	}

	public static String getCFRay(@NotNull HttpServletRequest request) {
		return cleanTaint(request.getHeader(HomeConstants.CF_HEADER_CF_RAY));
	}

	public static long getOpcalSequence(@NotNull HttpServletRequest request) {
		try {
			return Long.valueOf(cleanTaint(request.getHeader(HomeConstants.OPCAL_HEADER_SEQUENCE)));
		} catch (Exception e) {
			// do nothing
		}
		return 0;
	}

	public static String getOpcalSignature(@NotNull HttpServletRequest request) {
		return cleanTaint(request.getHeader(HomeConstants.OPCAL_HEADER_SIGNATURE));
	}

	public static String getOpcalDirectSign(@NotNull HttpServletRequest request) {
		return cleanTaint(request.getHeader(HomeConstants.OPCAL_HEADER_DIRECT_SIGN));
	}

	public static boolean isDirect(@NotNull HttpServletRequest request) {
		return getCFRay(request) == null;
	}

	public static String getRequestPath(@NotNull HttpServletRequest request) {
		String url = request.getServletPath();
		if (request.getPathInfo() != null) {
			url += request.getPathInfo();
		}
		return cleanTaint(url);
	}

}
