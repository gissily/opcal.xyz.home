package xyz.opcal.home.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HomeConstants {

	public static final String CF_HEADER_CONNECTING_IP = "CF-Connecting-IP";

	public static final String CF_HEADER_IP_COUNTRY = "CF-IPCountry";
	
	public static final String W_HEADER_CONNECTING_IP = "worker-connecting-ip";

	public static final String W_HEADER_IP_COUNTRY = "worker-ipcountry";

	public static final String CF_HEADER_CF_RAY = "CF-RAY";

	public static final String X_FORWARDED_FOR = "X-Forwarded-For";
	public static final String X_REAL_IP = "X-Real-IP";

	public static final String LOCALHOST_IP = "127.0.0.1";
	public static final String LOCALHOST = "localhost";

	public static final String OPCAL_HEADER_SEQUENCE = "opcal-sequence";
	public static final String OPCAL_HEADER_SIGNATURE = "opcal-signature";
	public static final String OPCAL_HEADER_DIRECT_SIGN = "opcal-direct-sign";

	public static final String DF_YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
	public static final String DF_YYYYMMDD = "yyyyMMdd";
	
	public static final String INIT_OPCAL_INIT_DATA = "opcal-init-data";
	public static final String INIT_OPCAL_INIT_JSON = "opcalJson";
}
