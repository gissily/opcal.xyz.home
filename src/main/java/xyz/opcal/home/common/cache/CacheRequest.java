package xyz.opcal.home.common.cache;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class CacheRequest<T> {

	private @NonNull String cacheIndex;
	private @NonNull String cacheKey;
	private @NonNull ICacheLoader<T> cacheLoader;

	private int cacheExpiredMinutes = 15;
	private int cacheNullExpiredMinutes = 15;
	private int cacheLockedSeconds = 30;
	private boolean cacheNull = true;
}
