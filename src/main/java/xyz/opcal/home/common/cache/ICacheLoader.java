package xyz.opcal.home.common.cache;

@FunctionalInterface
public interface ICacheLoader<T> {

	T load(String cacheKey);
}
