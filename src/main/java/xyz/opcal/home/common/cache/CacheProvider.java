package xyz.opcal.home.common.cache;

import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RKeys;
import org.redisson.api.RLock;
import org.redisson.api.RMapCache;
import org.redisson.api.RSetCache;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;

public class CacheProvider {

	private @Autowired RedissonClient redissonClient;

	public <T> void updateCache(CacheRequest<T> request, T cache) {
		String cacheIndex = request.getCacheIndex();
		String cacheKey = request.getCacheKey();
		RMapCache<String, T> mapCache = redissonClient.getMapCache(cacheIndex);
		if (cache != null) {
			mapCache.put(cacheKey, cache, request.getCacheExpiredMinutes(), TimeUnit.MINUTES);
		} else if (request.isCacheNull()) {
			getNullValueKeyCache(cacheIndex).add(cacheKey, request.getCacheNullExpiredMinutes(), TimeUnit.MINUTES);
		}
	}

	public <T> RMapCache<String, T> getMapCache(String cacheIndex) {
		return redissonClient.getMapCache(cacheIndex);
	}

	private RSetCache<String> getNullValueKeyCache(String cacheIndex) {
		return redissonClient.getSetCache(cacheIndex + "-null-value-cahce");
	}

	public <T> T getCache(CacheRequest<T> request) {
		String cacheIndex = request.getCacheIndex();
		String cacheKey = request.getCacheKey();
		if (StringUtils.isBlank(cacheKey)) {
			return null;
		}
		RMapCache<String, T> mapCache = getMapCache(cacheIndex);
		T cache = mapCache.get(cacheKey);

		if (cache == null && request.isCacheNull() && getNullValueKeyCache(cacheIndex).contains(cacheKey)) {
			return null;
		} else if (cache == null) {
			RLock lock = redissonClient.getFairLock(cacheIndex + "-" + cacheKey);
			try {
				lock.lock(request.getCacheLockedSeconds(), TimeUnit.SECONDS);
				cache = mapCache.get(cacheKey);
				if (cache != null) {
					return cache;
				}
				cache = request.getCacheLoader().load(cacheKey);
				if (cache != null) {
					mapCache.put(cacheKey, cache, request.getCacheExpiredMinutes(), TimeUnit.MINUTES);
				} else if (request.isCacheNull()) {
					getNullValueKeyCache(cacheIndex).add(cacheKey, request.getCacheNullExpiredMinutes(), TimeUnit.MINUTES);
				}
				return cache;
			} finally {
				lock.unlock();
			}
		}
		return cache;
	}

	public void cleanCache(String cacheIndex) {
		getMapCache(cacheIndex).clear();
		getNullValueKeyCache(cacheIndex).clear();
	}

	public void removeCache(String cacheIndex, String cacheKey) {
		if (cacheKey == null) {
			return;
		}
		getMapCache(cacheIndex).remove(cacheKey);
	}

	@Scheduled(fixedRate = 1000 * 60 * 60)
	public void cleanExpired() {
		RKeys keys = redissonClient.getKeys();
		keys.getKeysStream().forEach(keys::clearExpire);
	}

}
