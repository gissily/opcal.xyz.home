package xyz.opcal.home.common;

import java.util.Arrays;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import lombok.Getter;

@Getter
public enum ResultCode {

	SUCCESS("000000", "success"), //
	DISABLE_SERVICE("000000", "disable service"), //
	MISS_PARAM("100001", "missing requisite parameter"), //
	NOT_SUPPORT_PARAM("100002", "not support parameter"), //
	SERVICE_ERROR("500000", "service error"), //
	NOT_SUPPORT_ENV("500001", "not support env"), //
	NOT_SUPPORT_METHOD("50000", "not support method"), //
	UNKNOW("999999", "unknow exception");

	ResultCode(String code, String message) {
		this.code = code;
		this.message = message;
	}

	private final String code;
	private final String message;

	public static ResultCode get(String code) {
		Optional<ResultCode> rsCode = Arrays.stream(values()).filter(resultCode -> StringUtils.equals(code, resultCode.code)).findFirst();
		if (rsCode.isPresent()) {
			return rsCode.get();
		}
		return null;
	}

}
