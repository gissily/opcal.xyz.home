package xyz.opcal.home.common.http.wrapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class AuthRequestWrapper extends HttpServletRequestWrapper {

	private String requestId;
	private String requestBody;

	public AuthRequestWrapper(String requestId, HttpServletRequest request, String requestBody) {
		super(request);
		this.requestId = requestId;
		this.requestBody = requestBody;
	}

	public String getRequestId() {
		return requestId;
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {

		ByteArrayInputStream bais = new ByteArrayInputStream(requestBody.getBytes(StandardCharsets.UTF_8));

		return new ServletInputStream() {

			private boolean finished = false;

			@Override
			public int read() throws IOException {
				int data = bais.read();
				if (data == -1) {
					this.finished = true;
				}
				return data;
			}

			@Override
			public void setReadListener(ReadListener listener) {
				// do nothing
			}

			@Override
			public boolean isReady() {
				return true;
			}

			@Override
			public boolean isFinished() {
				return finished;
			}

			@Override
			public void close() throws IOException {
				AuthRequestWrapper.this.getRequest().getInputStream().close();
			}

		};
	}

	@Override
	public String toString() {
		return requestBody;
	}

}
