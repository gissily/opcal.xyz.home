package xyz.opcal.home.common.http.client;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.config.SyncConfig;
import xyz.opcal.home.model.dto.result.BaseResult;

@Slf4j
@Component
public class HomeApiClient {

	public static final String API_SYNCIPINFO = "/api/syncIpInfo";
	public static final String API_SYNCREJECT = "/api/syncReject";

	private @Autowired SyncConfig apiConfig;
	private @Autowired AuthServConfig authServConfig;
	private @Autowired ObjectMapper objectMapper;

	private RestTemplate restTemplate = new RestTemplate();

	public void syncIpInfo(List<IpInfo> ips) {
		allApi(toBody(ips), API_SYNCIPINFO);
	}

	public void syncReject(List<String> ips) {
		allApi(toBody(ips), API_SYNCREJECT);
	}

	String toBody(Object request) {

		try {
			return objectMapper.writeValueAsString(request);
		} catch (Exception e) {
			log.error("tobody error ", e);
		}
		return "";
	}

	void allApi(String body, String api) {
		String[] urls = apiConfig.getUrls();
		for (String url : urls) {
			try {
				api(body, url + api);
			} catch (Exception e) {
				log.error("url [{}] api [{}] request error: ", url, api, e);
			}
		}

	}

	BaseResult api(String body, String api) {
		MultiValueMap<String, String> headers = new HttpHeaders();
		long opcalSequence = System.currentTimeMillis();
		headers.set(HomeConstants.OPCAL_HEADER_SEQUENCE, String.valueOf(opcalSequence));
		headers.set(HomeConstants.OPCAL_HEADER_DIRECT_SIGN, DigestUtils.sha1Hex(opcalSequence + authServConfig.getLocationToken()));
		headers.set(HomeConstants.OPCAL_HEADER_SIGNATURE, DigestUtils.sha1Hex(opcalSequence + body + authServConfig.getApiToken()));
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> entity = new HttpEntity<>(body, headers);
		return restTemplate.postForEntity(api, entity, BaseResult.class).getBody();
	}
}
