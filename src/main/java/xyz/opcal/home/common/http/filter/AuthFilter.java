package xyz.opcal.home.common.http.filter;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.http.mathcer.PathMatcher;
import xyz.opcal.home.common.http.wrapper.AuthRequestWrapper;
import xyz.opcal.home.common.http.wrapper.AuthResponseWrapper;
import xyz.opcal.home.common.logback.LogbackConstants;
import xyz.opcal.home.common.util.RequestUtils;
import xyz.opcal.home.model.dto.config.AuthServConfig;

@Slf4j
@Component
public class AuthFilter extends OncePerRequestFilter {

	private @Autowired AuthServConfig authServConfig;
	private @Autowired ObjectMapper objectMapper;
	private PathMatcher checkMathcer;

	@PostConstruct
	public void init() {
		checkMathcer = new PathMatcher(authServConfig.getCheckPaths());
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return !checkMathcer.matches(request);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

		String ip = RequestUtils.getIp(request);

		long opcalSequence = RequestUtils.getOpcalSequence(request);
		String opcalSignature = RequestUtils.getOpcalSignature(request);
		String opcalDirectSign = RequestUtils.getOpcalDirectSign(request);

		boolean isReject = (System.currentTimeMillis() - opcalSequence) > 5 * 60 * 1000;

		boolean isAllow = authServConfig.isAllowIp(ip);

		String requestBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);

		if (!isReject && !isAllow) {
			isReject = !StringUtils.equals(DigestUtils.sha1Hex(opcalSequence + authServConfig.getLocationToken()), opcalDirectSign);
		}

		if (!isReject) {
			isReject = !StringUtils.equals(DigestUtils.sha1Hex(opcalSequence + requestBody + authServConfig.getApiToken()), opcalSignature);
		}

		if (isReject) {
			request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, HttpStatus.FORBIDDEN.value());
			request.getRequestDispatcher("/error").forward(request, response);
			return;
		}

		String parameters = objectMapper.writeValueAsString(request.getParameterMap());
		String requestPath = RequestUtils.getRequestPath(request);
		String method = request.getMethod();
		String requestId = getRequestId(request);
		log.info("url [{}] method [{}] request id [{}] requset parameter [{}] body [{}]", requestPath, method, requestId, parameters, requestBody);
		AuthRequestWrapper requestWrapper = null;
		AuthResponseWrapper responseWrapper = null;
		try {
			MDC.put(LogbackConstants.MDC_THREAD_ID, requestId);
			requestWrapper = new AuthRequestWrapper(requestId, request, requestBody);
			responseWrapper = new AuthResponseWrapper(requestId, response);
			filterChain.doFilter(requestWrapper, responseWrapper);
		} finally {
			log.info("url [{}] request id [{}]  response body [{}]", requestPath, requestId, responseWrapper);
			MDC.clear();
		}
	}

	String getRequestId(HttpServletRequest request) {
		String cfRay = RequestUtils.getCFRay(request);
		if (cfRay == null) {
			cfRay = UUID.randomUUID().toString();
		}
		return cfRay;
	}

}
