/**
 * 
 */
package xyz.opcal.home.common.http.mathcer;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import org.springframework.util.AntPathMatcher;

import xyz.opcal.home.common.util.RequestUtils;

public class PathMatcher {

	@NotNull
	private String[] patterns;

	private AntPathMatcher antPathMathcer;

	public PathMatcher(String[] patterns) {
		this.patterns = patterns;
		this.antPathMathcer = new AntPathMatcher();
	}

	public boolean matches(HttpServletRequest request) {
		return matches(RequestUtils.getRequestPath(request));
	}

	public boolean matches(String path) {
		for (String pattern : patterns) {
			if (antPathMathcer.match(pattern, path)) {
				return true;
			}
		}
		return false;
	}

}
