package xyz.opcal.home.common.http.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.DispatcherServlet;

import xyz.opcal.home.common.http.mathcer.PathMatcher;
import xyz.opcal.home.common.util.RequestUtils;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.param.IpPathParam;
import xyz.opcal.home.model.dto.param.QueryIpParam;
import xyz.opcal.home.service.IpService;
import xyz.opcal.home.service.LocalRejectService;

public class HomeDispatcherServlet extends DispatcherServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4587669175598651342L;

	private static Logger apiLogger = LoggerFactory.getLogger("apiLogger");
	private static Logger rejectLogger = LoggerFactory.getLogger("rejectLogger");

	private @Autowired AuthServConfig authServConfig;
	private @Autowired IpService ipService;
	private @Autowired LocalRejectService localRejectService;
	private PathMatcher rejectMathcer;
	private PathMatcher skipMathcer;
	private PathMatcher checkMathcer;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		rejectMathcer = new PathMatcher(authServConfig.getRejectPaths());
		skipMathcer = new PathMatcher(authServConfig.getSkipPaths());
		checkMathcer = new PathMatcher(authServConfig.getCheckPaths());
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (skipMathcer.matches(request)) {
			super.service(request, response);
		} else {
			homeService(request, response);
		}
	}

	void homeService(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ip = RequestUtils.getIp(request);
		String countryCode = RequestUtils.getCountryCode(request);
		String requestPath = RequestUtils.getRequestPath(request);

		boolean isDirect = RequestUtils.isDirect(request);
		QueryIpParam ipParam = new QueryIpParam();
		ipParam.setIp(ip);
		ipParam.setDirect(isDirect);
		IpInfo ipInfo = ipService.queryIp(ipParam);
		if (StringUtils.isBlank(countryCode) && ipInfo != null) {
			countryCode = ipInfo.getCountryCode();
		}

		boolean isFromWorker = RequestUtils.fromWorker(request);
		IpPathInfo ipPathInfo = ipService.addIpPath(IpPathParam.of(ip, requestPath, isDirect, isFromWorker));
		apiLogger.info("request path [{}] is direct[{}] fromworker [{}] come from ip [{}] country [{}]", requestPath, isDirect, isFromWorker, ip, countryCode);

		if ((!authServConfig.isAllowIp(ip) && isDirect && !checkMathcer.matches(request)) || rejectMathcer.matches(request) || ipService.reject(ip)) {
			ipService.addRejected(ip);
			localRejectService.analyzeLocalReject(ipPathInfo);
			request.setAttribute(RequestDispatcher.ERROR_STATUS_CODE, HttpStatus.FORBIDDEN.value());
			rejectLogger.info("reject request [{}] from ip [{}] country [{}]", requestPath, ip, countryCode);
			request.getRequestDispatcher("/error").forward(request, response);
			return;
		}

		super.service(request, response);
	}

}
