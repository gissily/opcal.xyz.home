package xyz.opcal.home.model.dto.config;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(AuthServConfig.PREFIX)
public class AuthServConfig {

	public static final String PREFIX = "opcal.serv.auth";

	private String[] notFilterPaths;

	private String[] allowIps;

	private String[] rejectPaths;

	private String[] checkPaths;

	private String[] skipPaths;

	private String apiToken;

	private String locationToken;

	public boolean isAllowIp(String ip) {
		return ArrayUtils.contains(allowIps, ip);
	}

}
