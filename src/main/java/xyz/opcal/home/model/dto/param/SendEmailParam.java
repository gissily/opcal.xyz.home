package xyz.opcal.home.model.dto.param;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SendEmailParam {

	private String personal;
	private List<String> to;
	private List<String> cc;
	private List<String> bcc;
	private String subject;
	private String text;
	private boolean isHtmlText;
	private List<EmailAttachment> attachments;
}
