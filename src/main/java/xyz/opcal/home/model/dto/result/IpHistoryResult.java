package xyz.opcal.home.model.dto.result;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.IpPathInfo;

@Setter
@Getter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpHistoryResult extends BaseResult {

	private IpPathInfo pathInfo;
	private IpInfo ipInfo;
	private Boolean localReject;
}
