package xyz.opcal.home.model.dto.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties(MailConfig.PREFIX)
@Getter
@Setter
public class MailConfig {

	public static final String PREFIX = "opcal.mail";

	private boolean enable;
	
	private List<String> notifyTos;
}
