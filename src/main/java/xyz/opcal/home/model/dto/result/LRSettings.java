package xyz.opcal.home.model.dto.result;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LRSettings extends BaseResult {

	private String[] countryCodes;
	private String[] isps;
	private String[] paths;
}
