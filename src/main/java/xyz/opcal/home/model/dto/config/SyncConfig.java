package xyz.opcal.home.model.dto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(SyncConfig.PREFIX)
public class SyncConfig {
	public static final String PREFIX = "opcal.home.sync";

	private String current;
	
	private String[] urls;
}
