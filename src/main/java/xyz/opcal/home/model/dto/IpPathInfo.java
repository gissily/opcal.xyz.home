package xyz.opcal.home.model.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpPathInfo {

	private String ip;
	private Set<String> paths;
	private Boolean isReject = false;
	private Long lastTime;
	private String lastTimedf;
	private Boolean direct = false;
	private Set<String> hostNames;
	private Boolean fromWorker = false;
}
