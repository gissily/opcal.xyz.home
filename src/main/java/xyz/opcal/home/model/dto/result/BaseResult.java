package xyz.opcal.home.model.dto.result;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import xyz.opcal.home.common.ResultCode;

@Getter
@Setter
@ToString
@JsonInclude(value = Include.NON_NULL)
public class BaseResult {

	private String code;
	private String message;

	private String requestId;
	private String exception;

	public void setResultCode(ResultCode resultCode) {
		this.code = resultCode.getCode();
		this.message = resultCode.getMessage();
	}

	public static BaseResult create(ResultCode resultCode) {
		BaseResult result = new BaseResult();
		result.setCode(resultCode.getCode());
		result.setMessage(resultCode.getMessage());
		return result;
	}
}
