package xyz.opcal.home.model.dto.param;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class IpPathParam {

	private String ip;
	private String path;
	private boolean isDirect;
	private boolean fromWorker;
}
