package xyz.opcal.home.model.dto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(LocalRejectConfig.PREFIX)
public class LocalRejectConfig {

	public static final String PREFIX = "opcal.local.reject";

	private String[] countryCodes;
	private String[] isps;
	private String[] paths;

	private String ip4RejectCmd;
	private String ip6RejectCmd;
	private String listRejectCmd;

}
