package xyz.opcal.home.model.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * <pre>
 * { 
 * 	"query": "24.48.0.1",
 * 	"status": "success",
 * 	"continent": "North America",
 * 	"continentCode": "NA",
 * 	"country": "Canada",
 * 	"countryCode": "CA",
 * 	"region": "QC",
 * 	"regionName": "Quebec",
 * 	"city": "Montreal",
 * 	"district": "",
 * 	"zip": "H1S",
 * 	"lat": 45.5808,
 * 	"lon": -73.5825,
 * 	"timezone": "America/Toronto",
 * 	"isp": "Le Groupe Videotron Ltee",
 * 	"org": "Videotron Ltee",
 * 	"as": "AS5769 Videotron Telecom Ltee",
 * 	"asname": "VIDEOTRON",
 * 	"mobile": false,
 * 	"proxy": false,
 * 	"hosting": false
 * }
 * </pre>
 *
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class IpInfo {

	private String query;
	private String status;
	private String continent;
	private String continentCode;
	private String country;
	private String countryCode;
	private String region;
	private String regionName;
	private String city;
	private String district;
	private String zip;
	private BigDecimal lat;
	private BigDecimal lon;
	private String timezone;
	private String isp;
	private String org;
	private String as;
	private String asname;
	private Boolean mobile;
	private Boolean proxy;
	private Boolean hosting;

	private Integer offset;
	private String currency;
	private String reverse;

	private Long qtime;
	private String qtimedf;
	private Boolean direct;

}
