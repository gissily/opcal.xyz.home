package xyz.opcal.home.model.dto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties(SchedulerConfig.PREFIX)
public class SchedulerConfig {

	public static final String PREFIX = "opcal.scheduler";
	
	private int poolSize = 5;
}
