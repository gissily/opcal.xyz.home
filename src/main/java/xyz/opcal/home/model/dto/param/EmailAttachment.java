package xyz.opcal.home.model.dto.param;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailAttachment {

	private String attachmentFilename;

	/**
	 * Base64 String
	 */
	private String attachmentContent;
}
