package xyz.opcal.home.model.dto;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpcalInitStates {

	private Map<String, Object> auth;
	private Map<String, Object> errors;

}
