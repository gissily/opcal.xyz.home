package xyz.opcal.home.controller.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.model.dto.OpcalInitStates;

@Controller
public class HomeController {

	private @Autowired ObjectMapper objectMapper;

	@SneakyThrows
	@GetMapping(value = { "/" })
	public String index(Model model) {
		Object initData = model.getAttribute(HomeConstants.INIT_OPCAL_INIT_DATA);
		if (initData == null) {
			initData = new OpcalInitStates();
			model.addAttribute(HomeConstants.INIT_OPCAL_INIT_DATA, initData);
		}
		model.addAttribute(HomeConstants.INIT_OPCAL_INIT_JSON, objectMapper.writeValueAsString(initData));
		return "index";
	}

}
