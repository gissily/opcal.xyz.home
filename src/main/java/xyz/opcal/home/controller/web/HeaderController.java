package xyz.opcal.home.controller.web;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

//@RestController
public class HeaderController {

	private @Autowired ObjectMapper objectMapper;

	@GetMapping(value = "/headers", produces = MediaType.APPLICATION_JSON_VALUE)
	public String headers(@RequestHeader HttpHeaders header) throws JsonProcessingException {
		LinkedHashMap<String, String> headerSorted = header.toSingleValueMap().entrySet().stream().sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey()))
				.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (t, u) -> u, LinkedHashMap::new));
		Map<String, Object> result = new HashMap<>();
		result.put("headers", headerSorted);
		return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
	}
}
