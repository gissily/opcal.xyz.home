package xyz.opcal.home.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.home.common.ResultCode;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.model.dto.result.LRSettings;
import xyz.opcal.home.service.LocalRejectSettingService;

@RestController
@RequestMapping("/api/localreject")
public class LocalRejectSettingController {

	private @Autowired LocalRejectSettingService localRejectSettingService;

	@PostMapping("/addCountryCode")
	public BaseResult addCountryCode(@RequestBody List<String> countryCodes) {
		countryCodes.forEach(localRejectSettingService::addCountryCode);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/addIsp")
	public BaseResult addIsp(@RequestBody List<String> isps) {
		isps.forEach(localRejectSettingService::addIsp);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/addPath")
	public BaseResult addPath(@RequestBody List<String> paths) {
		paths.forEach(localRejectSettingService::addPath);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/deleteCountryCode")
	public BaseResult deleteCountryCode(@RequestBody List<String> countryCodes) {
		countryCodes.forEach(localRejectSettingService::removeCountryCode);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/deleteIsp")
	public BaseResult deleteIsp(@RequestBody List<String> isps) {
		isps.forEach(localRejectSettingService::removeIsp);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/deletePath")
	public BaseResult deletePath(@RequestBody List<String> paths) {
		paths.forEach(localRejectSettingService::removePath);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@GetMapping("/lrSettings")
	public LRSettings lrSettings() {
		LRSettings lrSettings = new LRSettings();
		lrSettings.setResultCode(ResultCode.SUCCESS);
		lrSettings.setCountryCodes(localRejectSettingService.getCountryCodes());
		lrSettings.setIsps(localRejectSettingService.getIsps());
		lrSettings.setPaths(localRejectSettingService.getPaths());
		return lrSettings;
	}

}
