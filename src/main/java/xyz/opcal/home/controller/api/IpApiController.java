package xyz.opcal.home.controller.api;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.home.common.ResultCode;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.model.dto.result.IpHistoryResult;
import xyz.opcal.home.service.IpService;
import xyz.opcal.home.service.LocalRejectService;

@RestController
@RequestMapping("/api")
public class IpApiController {

	private @Autowired IpService ipService;
	private @Autowired LocalRejectService localRejectService;

	@PostMapping("/syncIpInfo")
	public BaseResult syncIpInfo(@RequestBody List<IpInfo> ips) {
		ips.forEach(ipService::addIpInfo);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/syncReject")
	public BaseResult syncReject(@RequestBody List<String> ips) {
		ips.forEach(ipService::addRejected);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/removeReject")
	public BaseResult removeReject(@RequestBody String ip) {
		ipService.removeRejected(ip);
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/ipHistory")
	public IpHistoryResult ipHistory(@RequestBody String ip) {
		IpHistoryResult result = new IpHistoryResult();
		result.setResultCode(ResultCode.SUCCESS);
		result.setPathInfo(ipService.getIpPath(ip));
		result.setLocalReject(localRejectService.isLocalReject(ip));
		ipService.ips(new HashSet<>(Arrays.asList(ip))).stream().findFirst().ifPresent(result::setIpInfo);
		return result;
	}
}
