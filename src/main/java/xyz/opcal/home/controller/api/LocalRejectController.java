package xyz.opcal.home.controller.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.ResultCode;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.service.LocalRejectService;
import xyz.opcal.home.task.LocalRejectTask;

@RestController
@RequestMapping("/api")
public class LocalRejectController {

	private @Autowired LocalRejectTask localRejectTask;
	private @Autowired LocalRejectService localRejectService;

	@PostMapping("/checkLocalReject")
	public BaseResult checkLocalReject(@RequestBody String dateTime) {
		localRejectTask.analyze(LocalDateTime.parse(dateTime, DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)));
		return BaseResult.create(ResultCode.SUCCESS);
	}

	@PostMapping("/addLocalReject")
	public BaseResult addLocalReject(@RequestBody List<String> ips) {
		ips.forEach(localRejectService::addLocalReject);
		return BaseResult.create(ResultCode.SUCCESS);
	}

}
