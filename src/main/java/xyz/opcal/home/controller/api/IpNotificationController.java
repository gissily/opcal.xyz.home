package xyz.opcal.home.controller.api;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.task.NotificationTask;

@RestController
@RequestMapping("/api/ip/check")
public class IpNotificationController {

	private @Autowired NotificationTask notificationTask;

	@PostMapping("/checkNotification")
	public BaseResult checkNotification(@RequestBody String checkTime) {
		LocalDateTime rangeTime = LocalDateTime.parse(checkTime, DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS));
		return notificationTask.ipNotify(rangeTime);
	}

}
