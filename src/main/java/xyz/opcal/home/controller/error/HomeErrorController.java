package xyz.opcal.home.controller.error;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.model.dto.OpcalInitStates;

@Controller
@RequestMapping("/error")
public class HomeErrorController extends AbstractErrorController {

	private @Autowired ObjectMapper objectMapper;

	@Autowired
	public HomeErrorController(ErrorAttributes errorAttributes) {
		super(errorAttributes);
	}

	@Override
	public String getErrorPath() {
		return null;
	}

	@SneakyThrows
	@RequestMapping(produces = MediaType.TEXT_HTML_VALUE)
	public String errorHtml(Model model, HttpServletRequest request, HttpServletResponse response) {
		response.setStatus(getStatus(request).value());
		OpcalInitStates initData = (OpcalInitStates) model.getAttribute(HomeConstants.INIT_OPCAL_INIT_DATA);
		if (initData == null) {
			initData = new OpcalInitStates();
			model.addAttribute(HomeConstants.INIT_OPCAL_INIT_DATA, initData);
		}
		initData.setErrors(Collections.unmodifiableMap(getErrorAttributes(request, ErrorAttributeOptions.defaults())));
		model.addAttribute(HomeConstants.INIT_OPCAL_INIT_JSON, objectMapper.writeValueAsString(initData));
		return "index";
	}

	@RequestMapping
	public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
		Map<String, Object> body = getErrorAttributes(request, ErrorAttributeOptions.defaults());
		return new ResponseEntity<>(body, getStatus(request));
	}

	@ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
	public ResponseEntity<String> mediaTypeNotAcceptable(HttpServletRequest request) {
		return ResponseEntity.status(getStatus(request)).build();
	}

}
