package xyz.opcal.home.task;

import java.time.LocalDateTime;
import java.time.ZoneId;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.service.IpService;
import xyz.opcal.home.service.LocalRejectService;

@Slf4j
@Component
public class LocalRejectTask {

	private @Autowired AuthServConfig authServConfig;
	private @Autowired LocalRejectService localRejectService;
	private @Autowired IpService ipService;

	@Scheduled(cron = "0 0 * * * *")
	public void analyze() {
		analyze(LocalDateTime.now().minusHours(1));
	}

	public synchronized void analyze(LocalDateTime startTime) {
		log.info("local reject check start time at [{}]", startTime);
		long lastHourEpochMilli = startTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
		ipService.ipPaths().stream() //
				.filter(pathInfo -> pathInfo.getLastTime() >= lastHourEpochMilli) //
				.filter(pathInfo -> !authServConfig.isAllowIp(pathInfo.getIp())) //
				.forEach(localRejectService::analyzeLocalReject);
		log.info("local reject check finished");
	}

}
