package xyz.opcal.home.task;

import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.redisson.api.RLock;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.model.dto.config.MailConfig;
import xyz.opcal.home.model.dto.config.SyncConfig;
import xyz.opcal.home.model.dto.param.EmailAttachment;
import xyz.opcal.home.model.dto.param.QueryIpParam;
import xyz.opcal.home.model.dto.param.SendEmailParam;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.service.EmailService;
import xyz.opcal.home.service.IpService;
import xyz.opcal.home.service.LocalRejectService;

@Slf4j
@Component
public class NotificationTask {

	@Qualifier("emailTemplateEngine")
	private @Autowired TemplateEngine emailTemplateEngine;
	private @Autowired SyncConfig syncConfig;
	private @Autowired MailConfig mailConfig;
	private @Autowired IpService ipService;
	private @Autowired LocalRejectService localRejectService;
	private @Autowired EmailService emailService;
	private @Autowired RedissonClient redisson;

	@Scheduled(cron = "0 30 0 * * *")
	public void ipNotification() {

		RLock fairLock = redisson.getFairLock(NotificationTask.class.getCanonicalName() + "-lock");
		RMapCache<String, String> runningFlags = redisson.getMapCache(NotificationTask.class.getCanonicalName() + "-running-flags");
		String flag = LocalDateTime.now().format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDD));
		try {
			fairLock.lock(30, TimeUnit.SECONDS);

			if (runningFlags.get(flag) != null) {
				log.info("ipNotification is running");
				return;
			}
			runningFlags.put(flag, flag, 30, TimeUnit.MINUTES);

			LocalDateTime lastDay = LocalDateTime.now().minusMinutes(30).minusDays(1);
			ipNotify(lastDay);

		} catch (Exception e) {
			runningFlags.remove(flag);
			log.error("notification error", e);
		} finally {
			fairLock.unlock();
			runningFlags.clearExpire();
			ipService.clearExpire();
		}
	}

	public BaseResult ipNotify(LocalDateTime rangeTime) {
		String rangeFlag = rangeTime.format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDD));
		long lastDayEpochMilli = rangeTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();

		ipService.ipPaths().stream().filter(pathInfo -> pathInfo.getLastTime() == null).map(IpPathInfo::getIp).collect(Collectors.toList())
				.forEach(ipService::removeIpPath);

		List<IpPathInfo> ipPaths = ipService.ipPaths().stream() //
				.filter(pathInfo -> pathInfo.getLastTime() >= lastDayEpochMilli) //
				.sorted(Comparator.comparingLong(IpPathInfo::getLastTime)) //
				.collect(Collectors.toList());

		Context ctx = new Context();
		ctx.setVariable("ips", ipService.ips(ipPaths.stream().map(IpPathInfo::getIp).collect(Collectors.toSet())));
		ctx.setVariable("ipPaths", ipPaths.stream().map(this::createIpPathData).collect(Collectors.toList()));
		String subject = MessageFormat.format("Daily Requests Notification From {0} {1}", syncConfig.getCurrent(), rangeFlag);
		ctx.setVariable("title", subject);
		String htmlContent = this.emailTemplateEngine.process("email/ipNotification_template.html", ctx);
		SendEmailParam sendEmailParam = new SendEmailParam();
		sendEmailParam.setPersonal("Opcal Home");
		sendEmailParam.setSubject(subject);
		sendEmailParam.setText(htmlContent);
		sendEmailParam.setHtmlText(true);
		sendEmailParam.setTo(mailConfig.getNotifyTos());

		EmailAttachment attachment = new EmailAttachment();
		attachment.setAttachmentContent(Base64.encodeBase64String(htmlContent.getBytes()));
		attachment.setAttachmentFilename(subject + ".html");

		sendEmailParam.setAttachments(Collections.singletonList(attachment));
		return emailService.sendEmail(sendEmailParam);
	}

	Map<String, Object> createIpPathData(IpPathInfo pathInfo) {
		String ip = pathInfo.getIp();
		IpInfo ipInfo = ipService.queryIp(QueryIpParam.of(ip, pathInfo.getDirect()));
		Map<String, Object> data = new HashMap<>();
		data.put("ip", ip);
		data.put("direct", pathInfo.getDirect());
		data.put("fromWorker", pathInfo.getFromWorker());
		data.put("isReject", pathInfo.getIsReject());
		data.put("hostNames", pathInfo.getHostNames());
		data.put("paths", pathInfo.getPaths());
		data.put("lastTimedf", pathInfo.getLastTimedf());
		data.put("country", ipInfo.getCountry());
		data.put("isp", ipInfo.getIsp());
		data.put("localReject", localRejectService.isLocalReject(ip));
		return data;
	}
}
