package xyz.opcal.home.task;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import xyz.opcal.home.common.http.client.HomeApiClient;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.service.IpService;

@Component
public class IpSyncTask {

	private static final long IP_SYNC_PERIOD = 90 * 60 * 1000L;

	private @Autowired IpService ipService;
	private @Autowired HomeApiClient homeApiClient;

	public void syncIps() {
		List<IpInfo> ips = ipService.ips().stream().filter(ipInfo -> (System.currentTimeMillis() - ipInfo.getQtime()) <= IP_SYNC_PERIOD)
				.collect(Collectors.toList());
		homeApiClient.syncIpInfo(ips);
	}

	public void syncRejected() {
		homeApiClient.syncReject(ipService.rejectedIps());
	}
}
