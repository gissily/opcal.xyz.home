package xyz.opcal.home.configuration;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.servlet.DispatcherServlet;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import xyz.opcal.home.common.http.servlet.HomeDispatcherServlet;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.config.LocalRejectConfig;
import xyz.opcal.home.model.dto.config.MailConfig;
import xyz.opcal.home.model.dto.config.SchedulerConfig;
import xyz.opcal.home.model.dto.config.SyncConfig;

@EnableScheduling
@EnableConfigurationProperties(value = { MailConfig.class, AuthServConfig.class, SchedulerConfig.class, SyncConfig.class, LocalRejectConfig.class })
@Configuration
@Import(value = { HomeSchedulingConfigurer.class, RedissonConfiguration.class })
public class HomeConfiguration {

	@Primary
	@Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
	public DispatcherServlet dispatcherServlet(WebMvcProperties webMvcProperties) {
		DispatcherServlet dispatcherServlet = new HomeDispatcherServlet();
		dispatcherServlet.setDispatchOptionsRequest(webMvcProperties.isDispatchOptionsRequest());
		dispatcherServlet.setDispatchTraceRequest(webMvcProperties.isDispatchTraceRequest());
		dispatcherServlet.setThrowExceptionIfNoHandlerFound(webMvcProperties.isThrowExceptionIfNoHandlerFound());
		dispatcherServlet.setPublishEvents(webMvcProperties.isPublishRequestHandledEvents());
		dispatcherServlet.setEnableLoggingRequestDetails(webMvcProperties.isLogRequestDetails());
		return dispatcherServlet;
	}

	@Bean(name = "homeTaskScheduler", destroyMethod = "shutdown")
	public ThreadPoolTaskScheduler taskScheduler(SchedulerConfig schedulerConfig) {
		ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
		taskScheduler.setPoolSize(schedulerConfig.getPoolSize());
		taskScheduler.setThreadNamePrefix("home-service-");
		taskScheduler.initialize();
		return taskScheduler;
	}

	@Bean(name = "emailTemplateEngine")
	public TemplateEngine emailTemplateEngine() {
		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(emailTemplateResolver());
		return templateEngine;
	}

	private ITemplateResolver emailTemplateResolver() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setOrder(Integer.valueOf(1));
		templateResolver.setResolvablePatterns(Collections.singleton("email/*"));
		templateResolver.setPrefix("templates/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("LEGACYHTML5");
		templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
		templateResolver.setCacheable(true);
		return templateResolver;
	}

}
