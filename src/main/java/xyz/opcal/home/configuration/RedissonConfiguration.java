package xyz.opcal.home.configuration;

import java.io.IOException;
import java.net.URL;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.util.ResourceUtils;

import xyz.opcal.home.common.cache.CacheProvider;

public class RedissonConfiguration {

	@Bean(destroyMethod = "shutdown")
	public RedissonClient redisson(@Value("${redisson.config:classpath:redisson.yml}") String redissonConfig) throws IOException {
		URL config = ResourceUtils.getURL(redissonConfig);
		return Redisson.create(Config.fromYAML(config));
	}

	@Bean
	public CacheProvider cacheProvider() {
		return new CacheProvider();
	}

}
