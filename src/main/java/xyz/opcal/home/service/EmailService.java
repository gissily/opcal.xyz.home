package xyz.opcal.home.service;

import xyz.opcal.home.model.dto.param.SendEmailParam;
import xyz.opcal.home.model.dto.result.BaseResult;

public interface EmailService {

	BaseResult sendEmail(SendEmailParam param);
}
