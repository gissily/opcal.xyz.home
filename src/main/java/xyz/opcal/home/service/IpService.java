package xyz.opcal.home.service;

import java.util.List;
import java.util.Set;

import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.model.dto.param.IpPathParam;
import xyz.opcal.home.model.dto.param.QueryIpParam;

public interface IpService {

	IpInfo queryIp(QueryIpParam ipParam);

	void addIpInfo(IpInfo ipInfo);

	List<IpInfo> ips();

	List<IpInfo> ips(Set<String> keys);

	boolean reject(String ip);

	void addRejected(String ip);

	void removeRejected(String ip);

	List<String> rejectedIps();

	List<IpPathInfo> ipPaths();

	IpPathInfo addIpPath(IpPathParam ipPathParam);

	void addIpPath(IpPathInfo ipPathInfo);

	void removeIpPath(String ip);

	IpPathInfo getIpPath(String ip);

	void clearExpire();
}
