package xyz.opcal.home.service;

import java.util.Set;

import xyz.opcal.home.model.dto.IpPathInfo;

public interface LocalRejectService {

	void addLocalReject(String ip);

	boolean isLocalReject(String ip);

	void runRejectCommand(String ip);

	Set<String> listLocalRejects();

	void analyzeLocalReject(IpPathInfo ipPathInfo);
}
