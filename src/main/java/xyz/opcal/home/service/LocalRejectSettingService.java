package xyz.opcal.home.service;

import java.util.Set;

public interface LocalRejectSettingService {

	boolean containtCountryCode(String code);

	boolean containtIsp(String ispName);

	boolean matchPath(String path);

	boolean matchPaths(Set<String> paths);

	void addCountryCode(String countryCode);

	void addIsp(String isp);

	void addPath(String path);

	void removeCountryCode(String countryCode);

	void removeIsp(String isp);

	void removePath(String path);

	String[] getCountryCodes();

	String[] getIsps();

	String[] getPaths();
}
