package xyz.opcal.home.service.impl;

import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.mail.internet.MimeMessage;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.ResultCode;
import xyz.opcal.home.model.dto.config.MailConfig;
import xyz.opcal.home.model.dto.param.EmailAttachment;
import xyz.opcal.home.model.dto.param.SendEmailParam;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.service.EmailService;

@Slf4j
@Service
public class EmailServiceImpl implements EmailService {

	private @Autowired JavaMailSenderImpl mailSender;
	private @Autowired MailConfig mailConfig;

	@Override
	public BaseResult sendEmail(SendEmailParam param) {
		BaseResult result = new BaseResult();

		if (!mailConfig.isEnable()) {
			result.setResultCode(ResultCode.DISABLE_SERVICE);
			return result;
		}

		try {
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, StandardCharsets.UTF_8.name());
			helper.setFrom(mailSender.getUsername(), param.getPersonal());
			List<String> to = param.getTo();
			helper.setTo(to.toArray(new String[to.size()]));
			helper.setSubject(param.getSubject());
			helper.setText(param.getText(), param.isHtmlText());

			if (!CollectionUtils.isEmpty(param.getCc())) {
				List<String> cc = param.getCc();
				helper.setCc(cc.toArray(new String[cc.size()]));
			}

			if (!CollectionUtils.isEmpty(param.getBcc())) {
				List<String> bcc = param.getBcc();
				helper.setBcc(bcc.toArray(new String[bcc.size()]));
			}

			if (!CollectionUtils.isEmpty(param.getAttachments())) {
				for (EmailAttachment emailAttachment : param.getAttachments()) {
					helper.addAttachment(emailAttachment.getAttachmentFilename(),
							new ByteArrayResource(Base64.decodeBase64(emailAttachment.getAttachmentContent())));
				}
			}
			mailSender.send(mimeMessage);
			result.setResultCode(ResultCode.SUCCESS);
		} catch (Exception e) {
			log.error("send email error: ", e);
			result.setCode("500009");
			result.setMessage(e.getMessage());
		}
		return result;
	}

}
