package xyz.opcal.home.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RQueue;
import org.redisson.api.RSet;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.model.dto.config.LocalRejectConfig;
import xyz.opcal.home.model.dto.config.SyncConfig;
import xyz.opcal.home.model.dto.param.QueryIpParam;
import xyz.opcal.home.service.IpService;
import xyz.opcal.home.service.LocalRejectService;
import xyz.opcal.home.service.LocalRejectSettingService;

@Slf4j
@Service
public class LocalRejectServiceImpl implements LocalRejectService {

	private static final String LOCAL_REJECT_IP_CACHE_PREFIX = "opcal.home.local.reject.ip.";
	private static final String LOCAL_REJECT_IP_REGISTER_INDEX = "opcal.home.local.register";
	private static final String LOCAL_REJECT_IP_QUEUE_PREFIX = "opcal.home.local.reject.queue.";

	private static final String QUOTE = "\"";

	private static final Logger localRejectLogger = LoggerFactory.getLogger("localRejectLogger");

	private @Autowired RedissonClient redisson;
	private @Autowired IpService ipService;
	private @Autowired LocalRejectConfig localRejectConfig;
	private @Autowired SyncConfig syncConfig;
	private @Autowired LocalRejectSettingService localRejectSettingService;

	private volatile boolean running;
	private AtomicLong waitCounter = new AtomicLong();
	private Object monitor = new Object();

	@PostConstruct
	public void init() {
		running = true;
		Long last = System.currentTimeMillis();
		registerMap().put(syncConfig.getCurrent(), last);
		log.info("register host [{}] [{}]", syncConfig.getCurrent(), last);
		Thread localRejectThread = new LocalRejectThread();
		localRejectThread.setDaemon(true);
		localRejectThread.start();
		redisson.getKeys().delete(StringUtils.substringBeforeLast(LOCAL_REJECT_IP_CACHE_PREFIX, "."));
	}

	long waitTime() {
		long time = 5 * waitCounter.incrementAndGet();
		if (waitCounter.get() > 5) {
			waitCounter.set(0);
		}
		return time;
	}

	@PreDestroy
	public void destroy() {
		running = false;
		Long last = registerMap().remove(syncConfig.getCurrent());
		log.info("remove register host [{}] [{}]", syncConfig.getCurrent(), last);
	}

	RMap<String, Long> registerMap() {
		return redisson.getMap(LOCAL_REJECT_IP_REGISTER_INDEX);
	}

	RSet<String> localRejects() {
		return redisson.getSet(LOCAL_REJECT_IP_CACHE_PREFIX + syncConfig.getCurrent());
	}

	@Override
	public void addLocalReject(String ip) {
		registerMap().keySet().stream().map(key -> LOCAL_REJECT_IP_QUEUE_PREFIX + key).forEach(queueIndex -> redisson.getQueue(queueIndex).add(ip));
		try {
			synchronized (monitor) {
				waitCounter.set(0);
				monitor.notifyAll();
				log.info("notifying local reject thread to handle ip {}", ip);
			}
		} catch (Exception e) {
			// do nothing
		}
	}

	@Override
	public boolean isLocalReject(String ip) {
		return localRejects().contains(ip);
	}

	@Override
	public void runRejectCommand(String ip) {
		Process command = null;
		try {
			String cmline = localRejectConfig.getIp4RejectCmd();
			InetAddress address = InetAddress.getByName(ip);
			if (address instanceof Inet6Address) {
				cmline = localRejectConfig.getIp6RejectCmd();
			}
			command = Runtime.getRuntime().exec(new String[] { "bash", ResourceUtils.getFile(cmline).getAbsolutePath(), ip });
			String processResult = StringUtils.trim(IOUtils.toString(command.getInputStream(), StandardCharsets.UTF_8));
			localRejectLogger.info("local reject ip [{}] result [{}]", ip, processResult);
			if (StringUtils.contains(processResult, "success")) {
				localRejects().add(ip);
			}
		} catch (Exception e) {
			log.error("local reject command error:", e);
		} finally {
			if (command != null) {
				command.destroy();
			}
		}
	}

	@Override
	public Set<String> listLocalRejects() {
		RSet<String> localRejects = localRejects();
		String timeFlag = LOCAL_REJECT_IP_CACHE_PREFIX + syncConfig.getCurrent() + "-timestamp";
		RLock fairLock = redisson.getFairLock(timeFlag + "-lock");
		try {
			fairLock.lock();
			RAtomicLong timestamp = redisson.getAtomicLong(timeFlag);
			if (timestamp.get() == 0) {
				timestamp.set(System.currentTimeMillis());
			}
			LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp.get()), ZoneId.systemDefault());
			if (Duration.between(date, LocalDateTime.now()).toMinutes() >= 30) {
				loadLocalRule(localRejects::add);
			}
		} finally {
			fairLock.unlock();
		}
		return localRejects.readAll();
	}

	void loadLocalRule(LocalRuleLoader localRuleLoader) {
		Process command = null;
		try {
			command = Runtime.getRuntime().exec(new String[] { "bash", ResourceUtils.getFile(localRejectConfig.getListRejectCmd()).getAbsolutePath() });
			readRejects(command.getInputStream()).forEach(localRuleLoader::addReject);
		} catch (Exception e) {
			log.error("list rejects command error:", e);
		} finally {
			if (command != null) {
				command.destroy();
			}
		}
	}

	Set<String> readRejects(InputStream inputStream) throws IOException {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
			return reader.lines().filter(StringUtils::isNotBlank).map(this::loadIp).filter(StringUtils::isNotBlank).collect(Collectors.toSet());
		}
	}

	/**
	 * <p>
	 * rule family="ipv4" source address="x.x.x.x" reject
	 * </p>
	 * 
	 * @param line
	 * @return
	 */
	String loadIp(String line) {
		String[] data = StringUtils.split(StringUtils.trim(line), " ");
		if (StringUtils.equals(data[data.length - 1], "reject")) {
			String address = data[data.length - 2];
			return StringUtils.substringAfterLast(StringUtils.substringBeforeLast(address, QUOTE), QUOTE);
		}
		return null;
	}

	@Override
	public void analyzeLocalReject(IpPathInfo ipPathInfo) {
		boolean isDirect = ipPathInfo.getDirect();
		if (!isDirect) {
			log.debug("ip [{}] is not direct", ipPathInfo.getIp());
			return;
		}
		List<PathAnalyzer> analyzer = new ArrayList<>();
		analyzer.add((pathInfo, ipInfo) -> pathInfo.getHostNames().size() > 1);
		analyzer.add((pathInfo, ipInfo) -> ipInfo != null && localRejectSettingService.containtCountryCode(ipInfo.getCountryCode()));
		analyzer.add((pathInfo, ipInfo) -> ipInfo != null && localRejectSettingService.containtIsp(ipInfo.getIsp()));
		analyzer.add((pathInfo, ipInfo) -> ipInfo != null && localRejectSettingService.matchPaths(pathInfo.getPaths()));

		IpInfo ipInfo = ipService.queryIp(QueryIpParam.of(ipPathInfo.getIp(), isDirect));
		Boolean reduce = analyzer.stream().reduce(Boolean.FALSE, (t, u) -> {
			if (Boolean.TRUE.equals(t)) {
				return t;
			}
			return u.analyze(ipPathInfo, ipInfo);
		}, (t, u) -> t || u);
		if (Boolean.TRUE.equals(reduce) && !isLocalReject(ipPathInfo.getIp())) {
			addLocalReject(ipPathInfo.getIp());
			log.info("ip [{}] will add to local reject queue", ipPathInfo.getIp());
		}
	}

	void handleLocalReject(String ip) {
		if (!listLocalRejects().contains(ip)) {
			runRejectCommand(ip);
		}
	}

	@FunctionalInterface
	interface PathAnalyzer {
		boolean analyze(IpPathInfo ipPathInfo, IpInfo ipInfo);
	}

	@FunctionalInterface
	interface LocalRuleLoader {
		void addReject(String reject);
	}

	class LocalRejectThread extends Thread {

		public LocalRejectThread() {
			String name = LOCAL_REJECT_IP_QUEUE_PREFIX + syncConfig.getCurrent() + ".thread";
			setName(name);
		}

		@Override
		public void run() {
			log.info("thread [{}] start", getName());
			while (running) {
				RQueue<String> queue = redisson.getQueue(LOCAL_REJECT_IP_QUEUE_PREFIX + syncConfig.getCurrent());
				String ip = queue.poll();
				try {
					if (ip == null) {
						synchronized (monitor) {
							if (queue.isEmpty()) {
								long waitingTime = waitTime() * 1000;
								monitor.wait(waitingTime);
							}
						}
						continue;
					}
					handleLocalReject(ip);
					waitCounter.set(0);
				} catch (Exception e) {
					queue.add(ip);
				}
			}
		}
	}

}
