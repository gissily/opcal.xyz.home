package xyz.opcal.home.service.impl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ArrayUtils;
import org.redisson.api.RLock;
import org.redisson.api.RMapCache;
import org.redisson.api.RSetCache;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.cache.CacheProvider;
import xyz.opcal.home.common.cache.CacheRequest;
import xyz.opcal.home.common.util.DescComparator;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.model.dto.config.SyncConfig;
import xyz.opcal.home.model.dto.param.IpPathParam;
import xyz.opcal.home.model.dto.param.QueryIpParam;
import xyz.opcal.home.service.IpService;

@Service
@Slf4j
public class IpServiceImpl implements IpService {

	private static final String IP_CACHE_INDEX = "opcal.home.cache.ip";
	private static final String IP_PATH_CACHE_INDEX = "opcal.home.cache.ip.path";
	private static final String IP_CACHE_REJECT_INDEX = "opcal.home.cache.ip.reject";
	private static final int IP_CACHE_EXPIRED = 30 * 24 * 60;
	private static final String[] LOCAL_IPS = new String[] { HomeConstants.LOCALHOST_IP, HomeConstants.LOCALHOST };

	@Value("${opcal.api.ip}")
	private String ipApi;

	private @Autowired CacheProvider cacheProvider;
	private @Autowired RedissonClient redisson;
	private @Autowired SyncConfig syncConfig;

	@Override
	public IpInfo queryIp(QueryIpParam ipParam) {
		String ip = ipParam.getIp();
		if (ArrayUtils.contains(LOCAL_IPS, ip)) {
			IpInfo localIp = new IpInfo();
			localIp.setQuery(ip);
			localIp.setAs(HomeConstants.LOCALHOST);
			localIp.setCountryCode(HomeConstants.LOCALHOST);
			localIp.setCountry(HomeConstants.LOCALHOST);
			return localIp;
		}
		return cacheProvider.getCache(getCacheRequest(ipParam));
	}

	CacheRequest<IpInfo> getCacheRequest(QueryIpParam ipParam) {
		CacheRequest<IpInfo> cacheRequest = CacheRequest.of(IP_CACHE_INDEX, ipParam.getIp(), key -> query(ipParam));
		cacheRequest.setCacheExpiredMinutes(IP_CACHE_EXPIRED);
		cacheRequest.setCacheLockedSeconds(60);
		cacheRequest.setCacheNullExpiredMinutes(1); // api limits 45 per minute
		return cacheRequest;

	}

	IpInfo query(QueryIpParam ipParam) {
		String ip = ipParam.getIp();
		try {
			RestTemplate restTemplate = new RestTemplate();
			Map<String, String> uriVariables = new HashMap<>();
			uriVariables.put("query", ip);
			ResponseEntity<IpInfo> response = restTemplate.getForEntity(ipApi, IpInfo.class, uriVariables);
			Optional<IpInfo> ipInfoOptional = Optional.ofNullable(response.getBody());
			ipInfoOptional.ifPresent(ipInfo -> {
				ipInfo.setDirect(ipParam.isDirect());
				ipInfo.setQtime(System.currentTimeMillis());
				ipInfo.setQtimedf(LocalDateTime.now().format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)));
			});
			if (ipInfoOptional.isPresent()) {
				return ipInfoOptional.get();
			}
		} catch (Exception e) {
			log.error("api query [{}] error: ", ip, e);
		}
		return null;
	}

	RMapCache<String, IpInfo> getIpCache() {
		return cacheProvider.getMapCache(IP_CACHE_INDEX);
	}

	@Override
	public void addIpInfo(@NotNull IpInfo ipInfo) {
		if (!getIpCache().containsKey(ipInfo.getQuery())) {
			QueryIpParam ipParam = new QueryIpParam();
			ipParam.setIp(ipInfo.getQuery());
			ipParam.setDirect(ipInfo.getDirect());
			cacheProvider.updateCache(getCacheRequest(ipParam), ipInfo);
		}
	}

	@Override
	public List<IpInfo> ips() {
		return getIpCache().values().stream().sorted(DescComparator.descComparator(Comparator.comparingLong(IpInfo::getQtime))).collect(Collectors.toList());
	}

	@Override
	public List<IpInfo> ips(Set<String> keys) {
		return getIpCache().getAll(keys).values().stream().sorted(DescComparator.descComparator(Comparator.comparingLong(IpInfo::getQtime)))
				.collect(Collectors.toList());
	}

	@Override
	public boolean reject(String ip) {
		return rejectCache().contains(ip);
	}

	RSetCache<String> rejectCache() {
		return redisson.getSetCache(IP_CACHE_REJECT_INDEX);
	}

	@Override
	public void addRejected(String ip) {
		RSetCache<String> rejectCache = rejectCache();
		if (!rejectCache.contains(ip)) {
			rejectCache.add(ip, 30, TimeUnit.DAYS);
		}
		Optional.ofNullable(getIpPathCache().get(ip)).ifPresent(ipPathInfo -> {
			ipPathInfo.setIsReject(true);
			addIpPath(ipPathInfo);
		});
	}

	@Override
	public void removeRejected(String ip) {
		rejectCache().remove(ip);
		removeIpPath(ip);
	}

	@Override
	public List<String> rejectedIps() {
		return rejectCache().stream().collect(Collectors.toList());
	}

	RMapCache<String, IpPathInfo> getIpPathCache() {
		return cacheProvider.getMapCache(IP_PATH_CACHE_INDEX);
	}

	public void addIpPath(IpPathInfo ipPathInfo) {
		getIpPathCache().put(ipPathInfo.getIp(), ipPathInfo, IP_CACHE_EXPIRED, TimeUnit.MINUTES);
	}

	@Override
	public IpPathInfo addIpPath(@NotNull IpPathParam ipPathParam) {
		String ip = ipPathParam.getIp();
		RLock fairLock = redisson.getFairLock(IP_PATH_CACHE_INDEX + "-" + ip + "-lock");
		try {
			fairLock.lock(10, TimeUnit.SECONDS);
			IpPathInfo ipPathInfo = getIpPathCache().get(ip);
			if (ipPathInfo == null) {
				ipPathInfo = new IpPathInfo();
				ipPathInfo.setIp(ip);
				ipPathInfo.setPaths(new LinkedHashSet<>());
				ipPathInfo.setIsReject(false);
			}
			ipPathInfo.setPaths(addElements(ipPathInfo.getPaths(), ipPathParam.getPath()));
			ipPathInfo.setHostNames(addElements(ipPathInfo.getHostNames(), syncConfig.getCurrent()));
			ipPathInfo.setDirect(ipPathParam.isDirect());
			ipPathInfo.setFromWorker(ipPathParam.isFromWorker());
			ipPathInfo.setLastTime(System.currentTimeMillis());
			ipPathInfo.setLastTimedf(LocalDateTime.now().format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)));
			addIpPath(ipPathInfo);
			return ipPathInfo;
		} finally {
			fairLock.unlock();
		}
	}

	<T> Set<T> addElements(Set<T> set, T element) {

		if (set == null) {
			set = new LinkedHashSet<>();
		}
		Collections.addAll(set, element);
		return set;
	}

	@Override
	public void removeIpPath(String ip) {
		getIpPathCache().remove(ip);
	}

	@Override
	public List<IpPathInfo> ipPaths() {
		return getIpPathCache().values().stream().collect(Collectors.toList());
	}

	@Override
	public IpPathInfo getIpPath(String ip) {
		return getIpPathCache().get(ip);
	}

	@Override
	public void clearExpire() {
		getIpCache().clearExpire();
		getIpPathCache().clearExpire();
		rejectCache().clearExpire();
	}

}
