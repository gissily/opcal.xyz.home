package xyz.opcal.home.service.impl;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RSet;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.AntPathMatcher;

import xyz.opcal.home.service.LocalRejectSettingService;

@Service
public class LocalRejectSettingServiceImpl implements LocalRejectSettingService {

	private static final String LOCAL_REJECT_COUNTRY_CODE = "opcal.home.local.reject.country-code";
	private static final String LOCAL_REJECT_ISP = "opcal.home.local.reject.isp";
	private static final String LOCAL_REJECT_PATH = "opcal.home.local.reject.path";

	private @Autowired RedissonClient redisson;

	private AntPathMatcher pathMatcher = new AntPathMatcher();

	RSet<String> getLRCountryCode() {
		return redisson.getSet(LOCAL_REJECT_COUNTRY_CODE);
	}

	RSet<String> getLRIsp() {
		return redisson.getSet(LOCAL_REJECT_ISP);
	}

	RSet<String> getLRPath() {
		return redisson.getSet(LOCAL_REJECT_PATH);
	}

	String[] rsetArray(RSet<String> rset) {
		Set<String> all = rset.readAll();
		return all.toArray(new String[rset.size()]);
	}

	@Override
	public String[] getCountryCodes() {
		return rsetArray(getLRCountryCode());
	}

	@Override
	public String[] getIsps() {
		return rsetArray(getLRIsp());
	}

	@Override
	public String[] getPaths() {
		return rsetArray(getLRPath());
	}

	@Override
	public boolean containtCountryCode(String code) {
		return getLRCountryCode().contains(code);
	}

	@Override
	public boolean containtIsp(String ispName) {
		for (String isp : getLRIsp()) {
			if (StringUtils.containsIgnoreCase(ispName, isp)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean matchPath(String path) {
		for (String pattern : getLRPath().readAll()) {
			if (pathMatcher.match(pattern, path)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean matchPaths(Set<String> paths) {
		for (String path : paths) {
			if (matchPath(path)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public void addCountryCode(String countryCode) {
		getLRCountryCode().add(countryCode);
	}

	@Override
	public void addIsp(String isp) {
		getLRIsp().add(isp);
	}

	@Override
	public void addPath(String path) {
		getLRPath().add(path);
	}

	@Override
	public void removeCountryCode(String countryCode) {
		getLRCountryCode().remove(countryCode);
	}

	@Override
	public void removeIsp(String isp) {
		getLRIsp().remove(isp);
	}

	@Override
	public void removePath(String path) {
		getLRPath().remove(path);
	}

}
