package xyz.opcal.home.controller.error;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/error/demo")
public class SomeErrorController {

	@PostMapping("/someError")
	public String someError() {
		throw new RuntimeException("some error here");
	}
}
