package xyz.opcal.home.controller.error;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@Slf4j
class ErrorPageTests {

	private HttpHost httpHost;

	@LocalServerPort
	int serverPort;

	@BeforeAll
	void init(@Autowired Environment environment) {
		httpHost = new HttpHost("localhost", serverPort);
	}

	@Test
	void errorPage() throws IOException {
		HttpRequest request = new HttpGet("/anothingpage");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(404, result.getStatusLine().getStatusCode());
		log.error("error page {}", EntityUtils.toString(result.getEntity()));
	}

}
