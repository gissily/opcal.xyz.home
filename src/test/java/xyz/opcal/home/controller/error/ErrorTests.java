package xyz.opcal.home.controller.error;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import lombok.extern.slf4j.Slf4j;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Execution(ExecutionMode.SAME_THREAD)
@Import(ErrorConfiguration.class)
@Slf4j
class ErrorTests {

	private @Autowired TestRestTemplate testRestTemplate;

	@Test
	@Order(1)
	void errorNotHtml() {
		ResponseEntity<String> response = testRestTemplate.postForEntity("/anothingpage", null, String.class);
		assertEquals(404, response.getStatusCode().value());
		log.error("error response: {}", response.getBody());
	}

	@Test
	@Order(2)
	void someError() {
		ResponseEntity<String> response = testRestTemplate.postForEntity("/error/demo/someError", null, String.class);
		assertEquals(500, response.getStatusCode().value());
		System.out.println(response.getStatusCode());
		log.error("error response: {}", response.getBody());
	}

	@Test
	void notAccepted() {
		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.ACCEPT, "error");
		HttpEntity<String> requestEntity = new HttpEntity<>(headers);
		ResponseEntity<String> response = testRestTemplate.postForEntity("/", requestEntity, String.class);
		assertEquals(406, response.getStatusCodeValue());
	}

}
