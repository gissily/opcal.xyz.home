package xyz.opcal.home.controller.error;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class ErrorConfiguration {

	@Bean
	public SomeErrorController someErrorController() {
		return new SomeErrorController();
	}
}
