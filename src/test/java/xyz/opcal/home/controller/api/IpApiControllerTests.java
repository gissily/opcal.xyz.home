package xyz.opcal.home.controller.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.common.http.client.HomeApiClient;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.param.QueryIpParam;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.model.dto.result.IpHistoryResult;
import xyz.opcal.home.service.IpService;

@Slf4j
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Execution(ExecutionMode.SAME_THREAD)
class IpApiControllerTests {

	static final String REJECT_IP = "12.32.51.90";
	static final String[] RANDOM_IP_SOURCES = { "192.241.225.16", "185.140.53.8", "65.49.20.68", "106.57.230.23", "210.34.72.171", "211.66.225.12",
			"2409:8714:682:10:2215:771f:7346:d22d" };

	private @Autowired TestRestTemplate testRestTemplate;
	private @Autowired IpService ipService;
	private @Autowired ObjectMapper objectMapper;
	private @Autowired AuthServConfig authServConfig;

	String ip;
	IpInfo ipInfo;

	@BeforeAll
	void init() {
		HomeTestUtils.setAuthServConfig(authServConfig);
		ip = "128.1.49.123";
		QueryIpParam ipParam = new QueryIpParam();
		ipParam.setIp(ip);
		ipParam.setDirect(true);
		ipInfo = ipService.queryIp(ipParam);
	}

	IpInfo randomIpInfo(boolean ipv6, boolean direct) {
		IpInfo source = ipService.queryIp(QueryIpParam.of(RANDOM_IP_SOURCES[ThreadLocalRandom.current().nextInt(RANDOM_IP_SOURCES.length)], false));
		IpInfo randomipInfo = new IpInfo();
		BeanUtils.copyProperties(source, randomipInfo);
		String ip = ipv6 ? HomeTestUtils.randomIpv6() : HomeTestUtils.randomIpv4();
		randomipInfo.setQuery(ip);
		randomipInfo.setDirect(direct);
		return randomipInfo;
	}

	@Test
	@Order(1)
	void testApi() throws Exception {

		ResponseEntity<BaseResult> response1 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(Collections.singletonList(ipInfo)),
				HomeApiClient.API_SYNCIPINFO, true);
		assertEquals(200, response1.getStatusCode().value());

		ResponseEntity<BaseResult> response2 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(Collections.singletonList(ip)),
				HomeApiClient.API_SYNCREJECT, false);
		assertEquals(200, response2.getStatusCode().value());

		ResponseEntity<BaseResult> response3 = HomeTestUtils.api(testRestTemplate, ip, "/api/removeReject", true);
		assertEquals(200, response3.getStatusCode().value());

		ResponseEntity<IpHistoryResult> response4 = HomeTestUtils.api(testRestTemplate, HttpMethod.POST, ip, "/api/ipHistory", IpHistoryResult.class, false);
		assertEquals(200, response4.getStatusCode().value());
		log.info("ipHistory result {}", response4.getBody());

		ResponseEntity<BaseResult> response5 = HomeTestUtils.api(testRestTemplate,
				objectMapper.writeValueAsString(
						Arrays.asList(randomIpInfo(false, false), randomIpInfo(true, false), randomIpInfo(true, true), randomIpInfo(false, true))),
				HomeApiClient.API_SYNCIPINFO, true);
		assertEquals(200, response5.getStatusCode().value());

		ResponseEntity<BaseResult> response6 = HomeTestUtils.api(testRestTemplate,
				objectMapper.writeValueAsString(Arrays.asList(HomeTestUtils.randomIpv6(), HomeTestUtils.randomIpv4())), HomeApiClient.API_SYNCREJECT, false);
		assertEquals(200, response6.getStatusCode().value());

	}

	@Test
	@Order(2)
	void testDelay() throws IOException {
		MultiValueMap<String, String> headers = new HttpHeaders();
		String body = objectMapper.writeValueAsString(Collections.singletonList(ipInfo));
		long opcalSequence = 1;
		headers.set(HomeConstants.OPCAL_HEADER_SEQUENCE, String.valueOf(opcalSequence));
		headers.set(HomeConstants.OPCAL_HEADER_DIRECT_SIGN, DigestUtils.sha1Hex(opcalSequence + authServConfig.getLocationToken()));
		headers.set(HomeConstants.OPCAL_HEADER_SIGNATURE, DigestUtils.sha1Hex(opcalSequence + body + authServConfig.getApiToken()));
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HomeConstants.CF_HEADER_CONNECTING_IP, REJECT_IP);
		headers.set(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		headers.set(HomeConstants.X_FORWARDED_FOR, REJECT_IP);
		headers.set(HomeConstants.X_REAL_IP, REJECT_IP);
		headers.set(HomeConstants.CF_HEADER_IP_COUNTRY, "CN");

		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		ResponseEntity<BaseResult> response = testRestTemplate.postForEntity(HomeApiClient.API_SYNCIPINFO, entity, BaseResult.class);

		assertEquals(403, response.getStatusCode().value());
	}

	@Test
	@Order(3)
	void testNoSignature() throws IOException {
		MultiValueMap<String, String> headers = new HttpHeaders();
		String body = objectMapper.writeValueAsString(Collections.singletonList(ipInfo));
		long opcalSequence = System.currentTimeMillis();
		headers.set(HomeConstants.OPCAL_HEADER_SEQUENCE, String.valueOf(opcalSequence));
		headers.set(HomeConstants.OPCAL_HEADER_DIRECT_SIGN, DigestUtils.sha1Hex(opcalSequence + authServConfig.getLocationToken()));
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HomeConstants.CF_HEADER_CONNECTING_IP, REJECT_IP);
		headers.set(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		headers.set(HomeConstants.X_FORWARDED_FOR, REJECT_IP);
		headers.set(HomeConstants.X_REAL_IP, REJECT_IP);
		headers.set(HomeConstants.CF_HEADER_IP_COUNTRY, "CN");

		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		ResponseEntity<BaseResult> response = testRestTemplate.postForEntity(HomeApiClient.API_SYNCIPINFO, entity, BaseResult.class);

		assertEquals(403, response.getStatusCode().value());
	}

	@Test
	@Order(4)
	void testNoDirectSign() throws IOException {
		MultiValueMap<String, String> headers = new HttpHeaders();
		String body = objectMapper.writeValueAsString(Collections.singletonList(ipInfo));
		long opcalSequence = System.currentTimeMillis();
		headers.set(HomeConstants.OPCAL_HEADER_SEQUENCE, String.valueOf(opcalSequence));

		headers.set(HomeConstants.OPCAL_HEADER_SIGNATURE, DigestUtils.sha1Hex(opcalSequence + body + authServConfig.getApiToken()));
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		headers.set(HomeConstants.CF_HEADER_CONNECTING_IP, REJECT_IP);
		headers.set(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		headers.set(HomeConstants.X_FORWARDED_FOR, REJECT_IP);
		headers.set(HomeConstants.X_REAL_IP, REJECT_IP);
		headers.set(HomeConstants.CF_HEADER_IP_COUNTRY, "CN");

		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		ResponseEntity<BaseResult> response = testRestTemplate.postForEntity(HomeApiClient.API_SYNCIPINFO, entity, BaseResult.class);

		assertEquals(403, response.getStatusCode().value());
	}

}
