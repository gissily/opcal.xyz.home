package xyz.opcal.home.controller.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.result.BaseResult;

@TestInstance(Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class IpNotificationControllerTests {

	private @Autowired TestRestTemplate testRestTemplate;
	private @Autowired AuthServConfig authServConfig;

	@BeforeAll
	void init() {
		HomeTestUtils.setAuthServConfig(authServConfig);
	}

	@Test
	void check() throws IOException {
		ResponseEntity<BaseResult> response1 = HomeTestUtils.api(testRestTemplate,
				LocalDateTime.now().format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)), "/api/ip/check/checkNotification", true);
		assertEquals(200, response1.getStatusCode().value());
	}
}
