package xyz.opcal.home.controller.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.config.LocalRejectConfig;
import xyz.opcal.home.model.dto.result.BaseResult;
import xyz.opcal.home.model.dto.result.LRSettings;
import xyz.opcal.home.service.LocalRejectSettingService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Execution(ExecutionMode.SAME_THREAD)
class LocalRejectSettingControllerTests {

	private @Autowired TestRestTemplate testRestTemplate;
	private @Autowired LocalRejectConfig localRejectConfig;
	private @Autowired ObjectMapper objectMapper;
	private @Autowired AuthServConfig authServConfig;
	private @Autowired LocalRejectSettingService localRejectSettingService;

	@BeforeAll
	void before() {
		HomeTestUtils.setAuthServConfig(authServConfig);
	}

	@Test
	@Order(1)
	void add() throws IOException {
		ResponseEntity<BaseResult> response1 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(localRejectConfig.getCountryCodes()),
				"/api/localreject/addCountryCode", true);
		assertEquals(200, response1.getStatusCode().value());
		ResponseEntity<BaseResult> response2 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(localRejectConfig.getIsps()),
				"/api/localreject/addIsp", false);
		assertEquals(200, response2.getStatusCode().value());
		ResponseEntity<BaseResult> response3 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(localRejectConfig.getPaths()),
				"/api/localreject/addPath", false);
		assertEquals(200, response3.getStatusCode().value());
	}

	@Test
	@Order(2)
	void remove() throws IOException {
		ResponseEntity<BaseResult> response1 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(Arrays.asList("CN")),
				"/api/localreject/deleteCountryCode", true);
		assertEquals(200, response1.getStatusCode().value());
		ResponseEntity<BaseResult> response2 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(Arrays.asList("TDI")),
				"/api/localreject/deleteIsp", true);
		assertEquals(200, response2.getStatusCode().value());
		ResponseEntity<BaseResult> response3 = HomeTestUtils.api(testRestTemplate, objectMapper.writeValueAsString(Arrays.asList("/myzu")),
				"/api/localreject/deletePath", true);
		assertEquals(200, response3.getStatusCode().value());
	}

	@Test
	@Order(3)
	void containt() {

		assertTrue(() -> localRejectSettingService.containtCountryCode("RU"));
		assertTrue(() -> localRejectSettingService.containtIsp("Zenlayer"));
		assertTrue(() -> localRejectSettingService.matchPath("/phpMyAdmin.php"));
		assertTrue(() -> localRejectSettingService.matchPaths(new HashSet<>(Arrays.asList("/noa", "/phpMyAdminfauas.das"))));

		assertFalse(() -> localRejectSettingService.containtCountryCode("CN"));
		assertFalse(() -> localRejectSettingService.containtIsp("ALi"));
		assertFalse(() -> localRejectSettingService.matchPath("/index"));
		assertFalse(() -> localRejectSettingService.matchPaths(new HashSet<>(Arrays.asList("/noa", "/ninadas"))));

	}

	@Test
	@Order(4)
	void getLRSetting() throws IOException {
		ResponseEntity<LRSettings> response = HomeTestUtils.api(testRestTemplate, HttpMethod.GET, "", "/api/localreject/lrSettings", LRSettings.class, false);
		assertEquals(200, response.getStatusCode().value());
		assertNotNull(response.getBody());
		assertNotNull(response.getBody().getCountryCodes());
		assertNotNull(response.getBody().getIsps());
		assertNotNull(response.getBody().getPaths());

	}

}
