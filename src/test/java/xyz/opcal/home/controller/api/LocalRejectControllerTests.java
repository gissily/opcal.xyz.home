package xyz.opcal.home.controller.api;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.ObjectMapper;

import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.result.BaseResult;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Execution(ExecutionMode.SAME_THREAD)
class LocalRejectControllerTests {

	private @Autowired TestRestTemplate testRestTemplate;
	private @Autowired AuthServConfig authServConfig;
	private @Autowired ObjectMapper objectMapper;

	@BeforeAll
	void before() {
		HomeTestUtils.setAuthServConfig(authServConfig);
	}

	@Test
	@Order(1)
	void checkLocalReject() throws Exception {

		ResponseEntity<BaseResult> response = HomeTestUtils.api(testRestTemplate,
				LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0)).format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)),
				"/api/checkLocalReject", false);
		assertEquals(200, response.getStatusCode().value());
	}

	@Test
	@Order(2)
	void addLocalReject() throws Exception {
		ResponseEntity<BaseResult> response = HomeTestUtils.api(testRestTemplate,
				objectMapper.writeValueAsString(Arrays.asList(HomeTestUtils.randomIpv4(), HomeTestUtils.randomIpv4(), HomeTestUtils.randomIpv6())),
				"/api/addLocalReject", true);
		assertEquals(200, response.getStatusCode().value());
	}
}
