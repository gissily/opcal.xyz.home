package xyz.opcal.home.controller.web;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.UUID;

import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.service.IpService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Execution(ExecutionMode.SAME_THREAD)
@Slf4j
class WebPageTests {

	static final String IP = "219.137.119.260";

	private HttpHost httpHost;

	@LocalServerPort
	int serverPort;

	@BeforeAll
	void init(@Autowired Environment environment) {
		httpHost = new HttpHost("localhost", serverPort);
	}

	@AfterAll
	void close(@Autowired IpService ipService) {
		ipService.removeRejected(IP);
	}

	@Test
	@Order(0)
	void home() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("home page {}", EntityUtils.toString(result.getEntity()));
	}

	@Test
	@Order(1)
	void mockHeader1() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		request.addHeader(HomeConstants.CF_HEADER_CONNECTING_IP, IP);
		request.addHeader(HomeConstants.CF_HEADER_IP_COUNTRY, "CN");
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("header1 finished");
	}

	@Test
	@Order(2)
	void mockHeader2() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.X_FORWARDED_FOR, IP);
		request.addHeader(HomeConstants.X_REAL_IP, IP);
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("header2 finished");
	}

	@Test
	@Order(3)
	void mockHeader3() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.X_FORWARDED_FOR, IP);
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("header3 finished");
	}

	@Test
	@Order(4)
	void mockHeader4() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.X_FORWARDED_FOR, HomeConstants.LOCALHOST_IP);
		request.addHeader(HomeConstants.X_REAL_IP, IP);
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("header4 finished");
	}

	@Test
	@Order(5)
	void mockHeader5() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.X_FORWARDED_FOR, HomeConstants.LOCALHOST_IP);
		request.addHeader(HomeConstants.X_REAL_IP, IP + "," + HomeConstants.LOCALHOST_IP);
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("header5 finished");
	}

	@Test
	@Order(6)
	void mockHeader6() throws IOException {
		HttpRequest request = new HttpGet("/");
		request.addHeader(HttpHeaders.ACCEPT, MediaType.TEXT_HTML_VALUE);
		request.addHeader(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		request.addHeader(HomeConstants.W_HEADER_CONNECTING_IP, IP);
		request.addHeader(HomeConstants.W_HEADER_IP_COUNTRY, "CN");
		HttpResponse result = HttpClients.createDefault().execute(httpHost, request);
		assertEquals(200, result.getStatusLine().getStatusCode());
		log.info("header6 finished");
	}
}
