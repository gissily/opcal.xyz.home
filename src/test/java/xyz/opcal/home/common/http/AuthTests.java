package xyz.opcal.home.common.http;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedHashSet;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.common.ResultCode;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.service.IpService;

@Slf4j
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestInstance(Lifecycle.PER_CLASS)
@Execution(ExecutionMode.SAME_THREAD)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AuthTests {

	static final String MOCK_IP = "12.89.3.4";
	static final String[] testIps = new String[] { "91.241.19.84", "111.224.7.19", "192.241.237.29", "40.87.95.185" };

	private @Autowired TestRestTemplate testRestTemplate;
	private @Autowired IpService ipService;

	@AfterAll
	void afterAll() {
		ipService.removeRejected(MOCK_IP);
		ipService.removeIpPath(MOCK_IP);
		for (String ip : testIps) {
			ipService.removeRejected(ip);
			ipService.removeIpPath(ip);
		}
	}

	@Test
	@Order(1)
	void denyTest() {

		HttpEntity<String> requestEntity = new HttpEntity<>(HomeTestUtils.cdnHeaders(MOCK_IP));
		ResponseEntity<String> response = testRestTemplate.exchange("/vpn-software/vpn-windows", HttpMethod.GET, requestEntity, String.class);
		assertEquals(403, response.getStatusCodeValue());

		assertDoesNotThrow(() -> testRestTemplate.exchange("/vpn-software/vpn-windows", HttpMethod.GET,
				new HttpEntity<>(HomeTestUtils.cdnHeaders(HomeTestUtils.randomIpv4())), String.class));
	}

	@Test
	@Order(2)
	void localRejectTest() {
		HttpEntity<String> requestEntity0 = new HttpEntity<>(HomeTestUtils.directHeaders(testIps[0]));
		ResponseEntity<String> response0 = testRestTemplate.exchange("/", HttpMethod.GET, requestEntity0, String.class);
		assertEquals(403, response0.getStatusCodeValue());

		String testip1 = testIps[1];
		IpPathInfo ipPathInfo = new IpPathInfo();
		ipPathInfo.setIp(testip1);
		ipPathInfo.setPaths(new LinkedHashSet<>());
		ipPathInfo.setIsReject(false);
		ipPathInfo.setPaths(new LinkedHashSet<>(Arrays.asList("/")));
		ipPathInfo.setHostNames(new LinkedHashSet<>(Arrays.asList("host1", "host2")));
		ipPathInfo.setDirect(true);
		ipPathInfo.setLastTime(System.currentTimeMillis());
		ipPathInfo.setLastTimedf(LocalDateTime.now().format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)));
		ipService.addIpPath(ipPathInfo);

		HttpEntity<String> requestEntity1 = new HttpEntity<>(HomeTestUtils.directHeaders(testip1));
		ResponseEntity<String> response1 = testRestTemplate.exchange("/", HttpMethod.GET, requestEntity1, String.class);
		assertEquals(403, response1.getStatusCodeValue());

		HttpEntity<String> requestEntity2 = new HttpEntity<>(HomeTestUtils.directHeaders(testIps[2]));
		ResponseEntity<String> response2 = testRestTemplate.exchange("/", HttpMethod.GET, requestEntity2, String.class);
		assertEquals(403, response2.getStatusCodeValue());

		HttpEntity<String> requestEntity3 = new HttpEntity<>(HomeTestUtils.directHeaders(testIps[3]));
		ResponseEntity<String> response3 = testRestTemplate.exchange("/mysqladmin", HttpMethod.GET, requestEntity3, String.class);
		assertEquals(403, response3.getStatusCodeValue());
	}

	@Test
	@Order(3)
	void fakeIpHeader() {
		String fakeIp = "127.0.0.1, 157.230.175.48";

		HttpEntity<String> requestEntity0 = new HttpEntity<>(HomeTestUtils.directHeaders(fakeIp));
		ResponseEntity<String> response0 = testRestTemplate.exchange("/", HttpMethod.GET, requestEntity0, String.class);
		assertEquals(200, response0.getStatusCodeValue());

		String trueIp = "2408:2d33:1300:40:4de1:5025:294f:a3eb";
		HttpEntity<String> requestEntity1 = new HttpEntity<>(HomeTestUtils.cdnHeaders(trueIp));
		ResponseEntity<String> response1 = testRestTemplate.exchange("/", HttpMethod.GET, requestEntity1, String.class);
		assertEquals(200, response1.getStatusCodeValue());
	}

	@Test
	@Order(4)
	void codeTest() {
		assertEquals(ResultCode.SUCCESS, ResultCode.get("000000"));
		assertEquals(ResultCode.UNKNOW, ResultCode.get("999999"));
		assertNull(ResultCode.get("empty_code"));
		log.info("code {} message {}", ResultCode.UNKNOW.getCode(), ResultCode.UNKNOW.getMessage());
	}

}
