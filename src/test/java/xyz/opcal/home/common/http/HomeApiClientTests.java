package xyz.opcal.home.common.http;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import xyz.opcal.home.common.http.client.HomeApiClient;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.config.SyncConfig;
import xyz.opcal.home.model.dto.param.QueryIpParam;
import xyz.opcal.home.service.IpService;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Execution(ExecutionMode.SAME_THREAD)
class HomeApiClientTests {

	private @Autowired HomeApiClient homeApiClient;
	private @Autowired IpService ipService;
	private @Autowired SyncConfig syncConfig;

	@LocalServerPort
	int randomServerPort;

	@Test
	void clientTest() {
		String url = "http://127.0.0.1:" + randomServerPort + "/";
		syncConfig.setUrls(new String[] { url });
		String ip = "128.1.49.123";
		QueryIpParam ipParam = new QueryIpParam();
		ipParam.setIp(ip);
		ipParam.setDirect(true);
		IpInfo ipInfo = ipService.queryIp(ipParam);
		assertDoesNotThrow(() -> homeApiClient.syncIpInfo(Collections.singletonList(ipInfo)));
		assertDoesNotThrow(() -> homeApiClient.syncReject(Collections.singletonList(ip)));
	}
}
