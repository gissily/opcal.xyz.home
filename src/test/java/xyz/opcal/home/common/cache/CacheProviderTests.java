package xyz.opcal.home.common.cache;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Execution(ExecutionMode.SAME_THREAD)
class CacheProviderTests {
	public static final String TEST_CACHE_INDEX = "test-cahce-index";

	private static Random random = new Random();

	private @Autowired CacheProvider cacheProvider;

	@Test
	@Order(1)
	@DisplayName("cache")
	@Description("cache tests")
	void testCache() {

		ICacheLoader<Long> cacheLoader = (key) -> {
			if (StringUtils.contains(key, "null")) {
				return null;
			}
			return random.nextLong();
		};
		CacheRequest<Long> cacheRequest = CacheRequest.of(TEST_CACHE_INDEX, "1", cacheLoader);
		Long cache = cacheProvider.getCache(cacheRequest);
		assertNotNull(cache);

		CacheRequest<Long> nullCacheRequest = CacheRequest.of(TEST_CACHE_INDEX, "nullKey", cacheLoader);
		Long nullCache = cacheProvider.getCache(nullCacheRequest);
		assertNull(nullCache);

		cacheProvider.updateCache(cacheRequest, 10000223L);

		nullCacheRequest.setCacheNull(true);
		cacheProvider.updateCache(nullCacheRequest, 100L);

		cacheProvider.getCache(nullCacheRequest);
		CacheRequest<Long> emptyKeyRequest = CacheRequest.of(TEST_CACHE_INDEX, "", cacheLoader);
		cacheProvider.getCache(emptyKeyRequest);
		cacheProvider.updateCache(nullCacheRequest, random.nextLong());

		String randomkey1 = String.valueOf(random.nextInt());
		CacheRequest<Long> randomKeyRequest1 = CacheRequest.of(TEST_CACHE_INDEX, randomkey1, cacheLoader);
		randomKeyRequest1.setCacheNull(true);
		cacheProvider.updateCache(randomKeyRequest1, null);
		randomKeyRequest1.setCacheNull(false);
		cacheProvider.updateCache(randomKeyRequest1, null);

		List<Integer> numberList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

		String randomkey2 = String.valueOf(random.nextInt());
		CacheRequest<Long> randomKeyRequest2 = CacheRequest.of(TEST_CACHE_INDEX, randomkey2, key -> null);
		randomKeyRequest2.setCacheNull(true);
		numberList.parallelStream().forEach(i -> cacheProvider.getCache(randomKeyRequest2));

		String randomkey3 = String.valueOf(random.nextInt());
		CacheRequest<Long> randomKeyRequest3 = CacheRequest.of(TEST_CACHE_INDEX, randomkey3, cacheLoader);
		numberList.parallelStream().forEach(i -> cacheProvider.getCache(randomKeyRequest3));

		String randomkey4 = String.valueOf(random.nextInt());
		numberList.parallelStream().map(i -> {
			boolean flag = i % 2 == 0;
			ICacheLoader<Long> loader = flag ? cacheLoader : key -> null;
			CacheRequest<Long> randomKeyRequest4 = CacheRequest.of(TEST_CACHE_INDEX, randomkey4, loader);
			randomKeyRequest4.setCacheNull(flag);
			return randomKeyRequest4;
		}).forEach(cacheProvider::getCache);

		String randomkey5 = String.valueOf(random.nextInt());
		numberList.parallelStream().map(i -> {
			boolean flag = i % 2 == 0;
			ICacheLoader<Long> loader = flag ? cacheLoader : key -> null;
			CacheRequest<Long> randomKeyRequest5 = new CacheRequest<Long>();
			randomKeyRequest5.setCacheIndex(TEST_CACHE_INDEX);
			randomKeyRequest5.setCacheKey(randomkey5);
			randomKeyRequest5.setCacheLoader(loader);
			randomKeyRequest5.setCacheNull(flag);
			randomKeyRequest5.setCacheExpiredMinutes(20);
			randomKeyRequest5.setCacheNullExpiredMinutes(50);
			randomKeyRequest5.setCacheLockedSeconds(10);
			randomKeyRequest5.setCacheNull(flag);
			return randomKeyRequest5;
		}).forEach(cacheProvider::getCache);

		cacheProvider.removeCache(TEST_CACHE_INDEX, randomkey1);
		cacheProvider.removeCache(TEST_CACHE_INDEX, randomkey2);
		cacheProvider.removeCache(TEST_CACHE_INDEX, randomkey3);
		cacheProvider.removeCache(TEST_CACHE_INDEX, randomkey4);
		cacheProvider.removeCache(TEST_CACHE_INDEX, randomkey5);
		cacheProvider.removeCache(TEST_CACHE_INDEX, "1");
		cacheProvider.removeCache(TEST_CACHE_INDEX, "nullKey");
		cacheProvider.removeCache(TEST_CACHE_INDEX, "");
		cacheProvider.removeCache(TEST_CACHE_INDEX, null);
		cacheProvider.cleanCache(TEST_CACHE_INDEX);
		cacheProvider.cleanExpired();
	}

	@Test
	@Order(2)
	void nullTest() {
		assertThrows(NullPointerException.class, () -> CacheRequest.of(null, null, null));
		assertThrows(NullPointerException.class, () -> CacheRequest.of(TEST_CACHE_INDEX, null, null));
		assertThrows(NullPointerException.class, () -> CacheRequest.of(TEST_CACHE_INDEX, "", null));

		CacheRequest<Long> nullRequest = new CacheRequest<Long>();
		assertThrows(NullPointerException.class, () -> nullRequest.setCacheIndex(null));
		assertThrows(NullPointerException.class, () -> nullRequest.setCacheKey(null));
		assertThrows(NullPointerException.class, () -> nullRequest.setCacheLoader(null));

	}
}
