package xyz.opcal.home.common.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.mock.web.MockHttpServletRequest;

import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.common.HomeTestUtils;

@TestInstance(Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class RequestUtilsTests {

	@Test
	@Order(1)
	void cleanTaint() {
		assertEquals("a_b_c_d", RequestUtils.cleanTaint("a\nb\tc\rd"));
	}

	@Test
	@Order(2)
	void getIp() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.W_HEADER_CONNECTING_IP, HomeTestUtils.randomIpv4());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request1)));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		request2.addHeader(HomeConstants.CF_HEADER_CONNECTING_IP, HomeTestUtils.randomIpv4());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request2)));

		MockHttpServletRequest request3 = new MockHttpServletRequest("GET", "/");
		request3.addHeader(HomeConstants.X_FORWARDED_FOR, HomeTestUtils.randomIpv4());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request3)));

		MockHttpServletRequest request4 = new MockHttpServletRequest("GET", "/");
		request4.addHeader(HomeConstants.X_FORWARDED_FOR, HomeConstants.LOCALHOST_IP);
		request4.addHeader(HomeConstants.X_REAL_IP, HomeTestUtils.randomIpv4());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request4)));

		MockHttpServletRequest request5 = new MockHttpServletRequest("GET", "/");
		request5.addHeader(HomeConstants.X_REAL_IP,
				StringUtils.join(new String[] { HomeConstants.LOCALHOST_IP, HomeConstants.LOCALHOST_IP, HomeConstants.LOCALHOST_IP }, ","));
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request5)));

		MockHttpServletRequest request6 = new MockHttpServletRequest("GET", "/");
		request6.addHeader(HomeConstants.X_REAL_IP, StringUtils.join(new String[] { HomeTestUtils.randomIpv4(), HomeConstants.LOCALHOST_IP }, ","));
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request6)));

		MockHttpServletRequest request7 = new MockHttpServletRequest("GET", "/");
		request7.addHeader(HomeConstants.X_FORWARDED_FOR, HomeTestUtils.randomIpv4() + ",sdasdsa");
		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(request7)));

		assertTrue(StringUtils.isNotBlank(RequestUtils.getIp(new MockHttpServletRequest("GET", "/"))));
	}

	@Test
	@Order(3)
	void fromWorker() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.W_HEADER_CONNECTING_IP, HomeTestUtils.randomIpv4());
		assertTrue(RequestUtils.fromWorker(request1));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		assertFalse(RequestUtils.fromWorker(request2));
	}

	@Test
	@Order(4)
	void isIp() {
		assertFalse(RequestUtils.isIp(null));
		assertTrue(RequestUtils.isIp(HomeTestUtils.randomIpv4()));
		assertTrue(RequestUtils.isIp(HomeTestUtils.randomIpv6()));
	}

	@Test
	@Order(4)
	void getCountryCode() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.W_HEADER_IP_COUNTRY, "US");
		assertTrue(StringUtils.isNotBlank(RequestUtils.getCountryCode(request1)));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		request2.addHeader(HomeConstants.CF_HEADER_IP_COUNTRY, "US");
		assertTrue(StringUtils.isNotBlank(RequestUtils.getCountryCode(request2)));

		MockHttpServletRequest request3 = new MockHttpServletRequest("GET", "/");
		assertFalse(StringUtils.isNotBlank(RequestUtils.getCountryCode(request3)));
	}

	@Test
	@Order(5)
	void getCFRay() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getCFRay(request1)));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		assertFalse(StringUtils.isNotBlank(RequestUtils.getCFRay(request2)));
	}

	@Test
	@Order(6)
	void getOpcalSequence() {
		long seq = System.currentTimeMillis();
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.OPCAL_HEADER_SEQUENCE, seq);
		assertEquals(seq, RequestUtils.getOpcalSequence(request1));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		assertEquals(0, RequestUtils.getOpcalSequence(request2));
	}

	@Test
	@Order(7)
	void getOpcalSignature() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.OPCAL_HEADER_SIGNATURE, UUID.randomUUID().toString());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getOpcalSignature(request1)));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		assertFalse(StringUtils.isNotBlank(RequestUtils.getOpcalSignature(request2)));
	}

	@Test
	@Order(8)
	void getOpcalDirectSign() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.OPCAL_HEADER_DIRECT_SIGN, UUID.randomUUID().toString());
		assertTrue(StringUtils.isNotBlank(RequestUtils.getOpcalDirectSign(request1)));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		assertFalse(StringUtils.isNotBlank(RequestUtils.getOpcalDirectSign(request2)));
	}

	@Test
	@Order(9)
	void isDirect() {
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.addHeader(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		assertFalse(RequestUtils.isDirect(request1));

		MockHttpServletRequest request2 = new MockHttpServletRequest("GET", "/");
		assertTrue(RequestUtils.isDirect(request2));
	}

	@Test
	@Order(10)
	void getRequestPath() {
		String servletPath = "/oa";
		String pathInfo = "/login";
		MockHttpServletRequest request1 = new MockHttpServletRequest("GET", "/");
		request1.setServletPath(servletPath);
		assertEquals(servletPath, RequestUtils.getRequestPath(request1));

		request1.setPathInfo(pathInfo);
		assertEquals(servletPath + pathInfo, RequestUtils.getRequestPath(request1));
	}

}
