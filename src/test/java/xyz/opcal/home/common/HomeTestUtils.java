package xyz.opcal.home.common;

import java.io.IOException;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

import lombok.Setter;
import lombok.experimental.UtilityClass;
import xyz.opcal.home.model.dto.config.AuthServConfig;
import xyz.opcal.home.model.dto.result.BaseResult;

@UtilityClass
public class HomeTestUtils {

	private static char IPV6_SCOPE[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	@Setter
	private static AuthServConfig authServConfig;

	public static ResponseEntity<BaseResult> api(TestRestTemplate testRestTemplate, String body, String api, boolean mockCdn) throws IOException {
		return api(testRestTemplate, HttpMethod.POST, body, api, BaseResult.class, mockCdn);
	}

	public static <T extends BaseResult> ResponseEntity<T> api(TestRestTemplate testRestTemplate, HttpMethod httpMethod, String body, String api,
			Class<T> resultClass, boolean mockCdn) throws IOException {
		MultiValueMap<String, String> headers = new HttpHeaders();
		long opcalSequence = System.currentTimeMillis();
		headers.set(HomeConstants.OPCAL_HEADER_SEQUENCE, String.valueOf(opcalSequence));
		headers.set(HomeConstants.OPCAL_HEADER_DIRECT_SIGN, DigestUtils.sha1Hex(opcalSequence + authServConfig.getLocationToken()));
		headers.set(HomeConstants.OPCAL_HEADER_SIGNATURE, DigestUtils.sha1Hex(opcalSequence + body + authServConfig.getApiToken()));
		headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

		if (mockCdn) {
			headers.addAll(cdnHeaders(randomIpv4()));
		}

		HttpEntity<String> entity = new HttpEntity<String>(body, headers);
		return testRestTemplate.exchange(api, httpMethod, entity, resultClass);
	}

	public static HttpHeaders cdnHeaders(String ip) {
		HttpHeaders headers = new HttpHeaders();
		headers.set(HomeConstants.CF_HEADER_CF_RAY, UUID.randomUUID().toString());
		headers.set(HomeConstants.CF_HEADER_CONNECTING_IP, ip);
		headers.set(HomeConstants.X_FORWARDED_FOR, ip);
		headers.set(HomeConstants.X_REAL_IP, ip);
		headers.set(HomeConstants.CF_HEADER_IP_COUNTRY, "CN");
		return headers;
	}

	public static HttpHeaders directHeaders(String ip) {
		HttpHeaders headers = new HttpHeaders();
		headers.set(HomeConstants.X_FORWARDED_FOR, ip);
		return headers;
	}

	public static int addr() {
		Random random = new Random();
		return random.nextInt(254) + 1;
	}

	public static String randomIpv4() {
		return StringUtils.join(new int[] { addr(), addr(), addr(), addr() }, '.');
	}

	public static String randomIpv6() {
		return StringUtils.join(generateIPv6Address(), ':');
	}

	private static String[] generateIPv6Address() {
		String randAddress[] = { "", "", "", "", "", "", "", "" };
		Random random = new Random();
		for (int i = 0; i < randAddress.length; i++) {
			for (int j = 0; j < 4; j++) {
				randAddress[i] = randAddress[i] + IPV6_SCOPE[random.nextInt(IPV6_SCOPE.length)];
			}
		}
		return randAddress;
	}
}
