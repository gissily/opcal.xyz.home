package xyz.opcal.home.task;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import xyz.opcal.home.model.dto.config.SyncConfig;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Execution(ExecutionMode.SAME_THREAD)
class IpSyncTaskTests {

	private @Autowired SyncConfig syncConfig;
	private @Autowired IpSyncTask ipSyncTask;

	@LocalServerPort
	int randomServerPort;

	@Test
	void syncTest() {
		String url = "http://127.0.0.1:" + randomServerPort + "/";
		syncConfig.setUrls(new String[] { url });
		assertDoesNotThrow(() -> ipSyncTask.syncIps());
		assertDoesNotThrow(() -> ipSyncTask.syncRejected());
	}

}
