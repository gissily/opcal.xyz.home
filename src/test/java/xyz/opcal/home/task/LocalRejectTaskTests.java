package xyz.opcal.home.task;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.LinkedHashSet;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import xyz.opcal.home.common.HomeConstants;
import xyz.opcal.home.model.dto.IpPathInfo;
import xyz.opcal.home.model.dto.param.IpPathParam;
import xyz.opcal.home.service.IpService;

@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
@TestInstance(Lifecycle.PER_CLASS)
class LocalRejectTaskTests {

	static String[] directIps = new String[] { "216.218.206.68", "180.149.125.173", "180.149.125.173", "83.97.20.31", "128.14.134.170", "1.2.3.4",
			"2408:8648:1300:40:4de1:5025:294f:a3eb" };
	static String[] ips = new String[] { "124.235.138.115", "124.90.52.208", };
	static String reIp = "128.14.209.226";

	private @Autowired IpService ipService;
	private @Autowired LocalRejectTask localRejectTask;

	@BeforeAll
	void before() {
		IpPathInfo ipPathInfo = new IpPathInfo();
		ipPathInfo.setIp(reIp);
		ipPathInfo.setPaths(new LinkedHashSet<>());
		ipPathInfo.setIsReject(false);
		ipPathInfo.setPaths(new LinkedHashSet<>(Arrays.asList("/")));
		ipPathInfo.setHostNames(new LinkedHashSet<>(Arrays.asList("host1", "host2")));
		ipPathInfo.setDirect(true);
		ipPathInfo.setLastTime(System.currentTimeMillis());
		ipPathInfo.setLastTimedf(LocalDateTime.now().format(DateTimeFormatter.ofPattern(HomeConstants.DF_YYYYMMDDHHMMSS)));
		ipService.addIpPath(ipPathInfo);
		Arrays.stream(directIps).forEach(ip -> ipService.addIpPath(IpPathParam.of(ip, "/", true, false)));
		Arrays.stream(ips).forEach(ip -> ipService.addIpPath(IpPathParam.of(ip, "/", false, false)));
	}

	@AfterAll
	void after() {
		Arrays.stream(directIps).forEach(ipService::removeIpPath);
		Arrays.stream(ips).forEach(ipService::removeIpPath);
		ipService.removeIpPath(reIp);
	}

	@Test
	void task1() {
		assertDoesNotThrow(() -> localRejectTask.analyze());
	}

	@Test
	void task2() {
		assertDoesNotThrow(() -> localRejectTask.analyze());
	}
}
