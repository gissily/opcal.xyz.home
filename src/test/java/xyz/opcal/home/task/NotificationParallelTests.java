package xyz.opcal.home.task;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
@TestInstance(Lifecycle.PER_CLASS)
class NotificationParallelTests {

	private @Autowired NotificationTask notificationTask;

	@ParameterizedTest
	@ValueSource(ints = { 1, 2, 3, 4, 5 })
	@Execution(ExecutionMode.CONCURRENT)
	void notification() {
		notificationTask.ipNotification();
		assertThat(System.currentTimeMillis()).isPositive();
	}

}
