package xyz.opcal.home.task;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
class NotificationTaskTests {

	private @Autowired NotificationTask notificationTask;

	@Test
	void notification() {
		assertDoesNotThrow(() -> notificationTask.ipNotification());
	}

}
