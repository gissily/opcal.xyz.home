package xyz.opcal.home;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
class ApplicationTests {

	@Test
	void contextLoads() {
		assertThat(System.currentTimeMillis()).isPositive();
	}

}
