package xyz.opcal.home.service;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.model.dto.config.SyncConfig;

@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LocalRejectServiceTests {

	private static final String LOCAL_REJECT_IP_CACHE_PREFIX = "opcal.home.local.reject.ip.";

	private @Autowired LocalRejectService localRejectService;
	private @Autowired RedissonClient redisson;
	private @Autowired SyncConfig syncConfig;

	@Test
	@Order(0)
	void runRejectCommand() {
		assertDoesNotThrow(
				() -> Arrays.asList("180.149.125.173", "2408:8648:1300:40:4de1:5025:294f:a3eb").forEach(ip -> localRejectService.runRejectCommand(ip)));
	}

	@Test
	@Order(1)
	void add() {
		assertDoesNotThrow(() -> localRejectService.addLocalReject(HomeTestUtils.randomIpv4()));
	}

	@Test
	@Order(2)
	void isRejected() {
		assertDoesNotThrow(() -> localRejectService.isLocalReject(HomeTestUtils.randomIpv4()));
	}

	@Test
	@Order(3)
	void listRejects() {
		assertDoesNotThrow(() -> localRejectService.listLocalRejects());

		RAtomicLong timestamp = redisson.getAtomicLong(LOCAL_REJECT_IP_CACHE_PREFIX + syncConfig.getCurrent() + "-timestamp");
		timestamp.set(0);
		assertDoesNotThrow(() -> localRejectService.listLocalRejects());

		timestamp.set(System.currentTimeMillis());
		assertDoesNotThrow(() -> localRejectService.listLocalRejects());

		timestamp.set(LocalDateTime.now().minusMinutes(90).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
		assertDoesNotThrow(() -> localRejectService.listLocalRejects());
	}
}
