package xyz.opcal.home.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.ResultCode;
import xyz.opcal.home.model.dto.param.EmailAttachment;
import xyz.opcal.home.model.dto.param.SendEmailParam;
import xyz.opcal.home.model.dto.result.BaseResult;

@Slf4j
@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
class EmailServiceTests {

	private @Autowired EmailService emailService;

	@Test
	void send() {
		SendEmailParam param = new SendEmailParam();
		List<String> tos = Arrays.asList("opcal.xyz@outlook.com");
		param.setTo(tos);
		param.setBcc(tos);
		param.setCc(tos);
		param.setPersonal("DEMO");
		param.setSubject("load Spring" + System.currentTimeMillis());
		param.setText("opaaa from spring" + UUID.randomUUID());

		EmailAttachment attachment = new EmailAttachment();
		attachment.setAttachmentContent(Base64.encodeBase64String("attachment test".getBytes()));
		attachment.setAttachmentFilename("emailAttachmentFile.txt");

		param.setAttachments(Collections.singletonList(attachment));

		BaseResult result = emailService.sendEmail(param);
		log.info("send result [{}]", result);
		assertEquals(result.getCode(), ResultCode.SUCCESS.getCode());
	}
}
