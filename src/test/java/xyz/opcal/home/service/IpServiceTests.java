package xyz.opcal.home.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.extern.slf4j.Slf4j;
import xyz.opcal.home.common.HomeTestUtils;
import xyz.opcal.home.model.dto.IpInfo;
import xyz.opcal.home.model.dto.param.IpPathParam;
import xyz.opcal.home.model.dto.param.QueryIpParam;

@Slf4j
@SpringBootTest
@Execution(ExecutionMode.SAME_THREAD)
class IpServiceTests {

	private @Autowired IpService ipService;

	@Test
	void testQuery() {
		String[] ips = new String[] { "128.1.49.123", "107.150.103.95", "128.14.224.65", "121.57.12.199", "121.57.12.199" };
		for (String ip : ips) {
			QueryIpParam ipParam = new QueryIpParam();
			ipParam.setIp(ip);
			ipParam.setDirect(true);
			IpInfo ipInfo = ipService.queryIp(ipParam);
			log.info("ip info {}", ipInfo);
			assertNotNull(ipInfo);
		}
	}

	@Test
	void ipPath() {
		ipService.addIpPath(IpPathParam.of("107.150.103.94", "/shuut", true, false));
		ipService.addIpPath(IpPathParam.of("107.150.103.94", "/aaaw", true, false));

		IpPathParam randomIpPath = new IpPathParam();
		randomIpPath.setIp(HomeTestUtils.randomIpv4());
		randomIpPath.setPath("/aaaw");
		randomIpPath.setDirect(true);
		randomIpPath.setFromWorker(false);
		ipService.addIpPath(randomIpPath);
		ipService.addIpPath(IpPathParam.of(HomeTestUtils.randomIpv4(), "/vpn-software/vpn-windows", true, false));
		ipService.addIpPath(IpPathParam.of(HomeTestUtils.randomIpv6(), "/aaaw", true, false));

		ipService.ipPaths();
		assertThat(System.currentTimeMillis()).isPositive();
	}

	@Test
	void clear() {
		assertDoesNotThrow(() -> ipService.clearExpire());
	}

}
